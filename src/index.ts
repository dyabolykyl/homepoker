import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as helmet from 'helmet';
import * as http from 'http';
// import * as morgan from 'morgan';
import * as path from 'path';

import { PokerServer } from './PokerServer';
import { logger } from './Logger';

const DIST_DIR = path.join(__dirname, '../../client/dist');
// const PUBLIC_DIR = path.join(process.cwd(), 'public');
// const MOBILE_DIST_DIR = path.join(__dirname, '../../client2/dist_mobile');
const PORT = process.env.PORT || 8080;

logger.info(`Static directory: ${DIST_DIR}`);

const app = express();

app.use(helmet());
// app.use(morgan('combined'));

app.use(bodyParser.urlencoded({ 'extended': true })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json

// TODO: move to routes
app.use(express.static(DIST_DIR));
app.use('/mobile', express.static(DIST_DIR));
app.use('/game', express.static(DIST_DIR));
app.use('/fake', express.static(DIST_DIR));
app.get('/game', (req: express.Request, res: express.Response) => {
  res.sendFile('game.html', {
    root: DIST_DIR
  });
});
app.get('/mobile', (req: express.Request, res: express.Response) => {
  res.sendFile('mobile.html', {
    root: DIST_DIR
  });
});
app.get('/mobile/fake', (req: express.Request, res: express.Response) => {
  res.sendFile('fake-mobile.html', {
    root: DIST_DIR
  });
});
app.get('/fake', (req: express.Request, res: express.Response) => {
  res.sendFile('fake.html', {
    root: DIST_DIR
  });
});

const server: http.Server = app.listen(PORT, function () {
  logger.info(`The server is running on port: ${PORT}`);
});

logger.info('Starting game server');
const pokerServer = new PokerServer();
pokerServer.start(server);
