import * as winston from 'winston';

export const logger = new winston.Logger({
  transports: [
    new winston.transports.File({
      filename: 'poker-server.log',
      colorize: true,
      level: 'debug',
      json: false
    }),
  ],
  
});
