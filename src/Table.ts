import { ChatRequest, JoinTableRequest, PlayerActionRequest, UpdatePlayerStateRequest, UpdateViewRequest, ViewTableRequest } from '../client/src/common/socket/Request';
import { INVALID_REQUEST, INVALID_TABLE, Response, Result, SUCCESS, hasFailed } from '../client/src/common/socket/Response';

import { ConnectionManager } from './ConnectionManager';
import { logger } from './Logger';

export class Table {
  readonly id: string;
  host: string;
  players: Set<string>;
  viewers: Set<string>;
  private readonly connectionManager: ConnectionManager;

  constructor(id: string, host: string, connectionManager: ConnectionManager) {
    this.id = id;
    this.host = host;
    this.connectionManager = connectionManager;
    this.players = new Set();
    this.viewers = new Set();
  }

  async joinTable(req: JoinTableRequest): Promise<Response> {
    const hostConnection = this.connectionManager.getConnection(this.host);
    const uuid = req.uuid as string;
    if (!hostConnection) {
      logger.warn(`No host found for ${this.id}`);
      return INVALID_TABLE;
    }

    if (this.players.has(uuid)) {
      logger.info(`${uuid} already joined ${this.id}`);
      return SUCCESS;
    }

    const res = await hostConnection.emitJoinTable(req);
    logger.debug(`Received response for join game request: ${res}`);
    if (res.result === Result.SUCCESS) {
      logger.info(`Adding ${uuid} to table ${this.id}`);
      this.players.add(uuid);
    } else {
      logger.warn(`Join table request failed for ${uuid}, table ${this.id}`);
    }

    return res;
  }

  async sendChat(req: ChatRequest): Promise<Response> {
    const hostConnection = this.connectionManager.getConnection(this.host);
    const uuid = req.uuid as string;
    if (!hostConnection) {
      logger.warn(`No host found for ${this.id}. Rejecting chat request`);
      return INVALID_TABLE;
    }

    if (!this.players.has(uuid)) {
      logger.warn(`${uuid} is not in table ${this.id}. Rejecting chat request`);
      return SUCCESS;
    }

    const res = await hostConnection.sendChat(req);
    if (hasFailed(res)) {
      logger.warn(`Chat request failed for ${uuid}, table ${this.id}: ${res.result}`);
    }

    return res;
  }

  async registerViewer(req: ViewTableRequest): Promise<Response> {
    const hostConnection = this.connectionManager.getConnection(this.host);
    if (!hostConnection) {
      logger.warn(`No host found for ${this.id}. Rejecting chat request`);
      return INVALID_TABLE;
    }

    const viewerId = req.uuid as string;
    this.viewers.add(viewerId);
    logger.info(`Registered viewer for table ${this.id}`);
    return hostConnection.sendRegisterViewer(req);
  }

  updateViewers(req: UpdateViewRequest): Response {
    this.viewers.forEach(viewerId => {
      const connection = this.connectionManager.getConnection(viewerId);
      if (!connection) {
        logger.warn(`No connection found for viewer ${viewerId}`);
        return;
      }

      // Ignore responses
      logger.info(`Updating view for ${viewerId}`);
      connection.sendStateUpdate(req);
    });
    return SUCCESS;
  }

  async forwardPlayerAction(req: PlayerActionRequest): Promise<Response> {
    const hostConnection = this.connectionManager.getConnection(this.host);
    const uuid = req.uuid as string;
    if (!hostConnection) {
      logger.warn(`No host found for ${this.id}. Rejecting player action request`);
      return INVALID_TABLE;
    }

    if (!this.players.has(uuid)) {
      logger.warn(`${uuid} is not in table ${this.id}. Rejecting player action request`);
      return SUCCESS;
    }

    const res = await hostConnection.sendPlayerAction(req);
    if (hasFailed(res)) {
      logger.warn(`Player action request failed for ${uuid}, table ${this.id}: ${res.result}`);
    }

    return res;
  }

  async updatePlayerState(req: UpdatePlayerStateRequest): Promise<Response> {
    const hostConnection = this.connectionManager.getConnection(this.host);
    const uuid = req.uuid as string;
    if (!hostConnection || hostConnection.uuid !== uuid) {
      logger.warn(`No host found for ${this.id}. Rejecting request`);
      return INVALID_TABLE;
    }

    const state = req.fullPlayerState;
    if (!state || !state.playerState || !state.playerState.id) {
      logger.warn(`Invalid state. Rejecting request`);
      return INVALID_REQUEST;
    }

    const playerId = state.playerState.id;
    if (!this.players.has(playerId)) {
      logger.warn(`${playerId} is not in table ${this.id}. Rejecting request`);
      return INVALID_REQUEST;
    }

    const playerConnection = this.connectionManager.getConnection(playerId);
    if (!playerConnection) {
      logger.warn(`${playerId} does not have an active connection. Rejecting request`);
      return INVALID_REQUEST;
    }

    const res = await playerConnection.sendUpdatePlayerState(req);
    if (hasFailed(res)) {
      logger.warn(`Update player state request failed for ${playerId}, table ${this.id}: ${res.result}`);
    }

    return res;
  }
}
