import { ChatRequest, HostTableRequest, JoinTableRequest, PlayerActionRequest, Request, SocketEvent, UpdatePlayerStateRequest, UpdateViewRequest, ViewTableRequest } from '../client/src/common/socket/Request';
import { INVALID_REQUEST, Response, SUCCESS, TIMEOUT } from '../client/src/common/socket/Response';

import { ConnectionManager } from './ConnectionManager';
import { PokerServer } from './PokerServer';
import { TableManager } from './TableManager';
import { logger } from './Logger';

type Callback<R extends Response> = (res: R) => void;
type RequestHandler<T extends Request, R extends Response> = (req: T) => Promise<R>;

export class Connection {

  uuid: string;

  readonly socket: SocketIO.Socket;
  private readonly tableManager: TableManager;
  private readonly connectionManager: ConnectionManager;
  private readonly pokerServer: PokerServer;

  constructor(socket: SocketIO.Socket, tableManager: TableManager, connectionManager: ConnectionManager, pokerServer: PokerServer) {
    this.socket = socket;
    this.tableManager = tableManager;
    this.connectionManager = connectionManager;
    this.pokerServer = pokerServer;

    socket.on('disconnect', this.onDisconnect);
    this.registerRequestHandler(SocketEvent.SET_UUID, this.handleSetUuid);
    this.registerRequestHandler(SocketEvent.HOST_TABLE, this.handleHostTable);
    this.registerRequestHandler(SocketEvent.JOIN_TABLE, this.handleJoinTable);
    this.registerRequestHandler(SocketEvent.CHAT, this.handleChat);
    this.registerRequestHandler(SocketEvent.VIEW_TABLE, this.handleViewTable);
    this.registerRequestHandler(SocketEvent.UPDATE_VIEW, this.handleUpdateView);
    this.registerRequestHandler(SocketEvent.PLAYER_ACTION, this.handlePlayerAction);
    this.registerRequestHandler(SocketEvent.UPDATE_PLAYER_STATE, this.handleUpdatePlayerState);
  }

  close() {

  }

  emitJoinTable(req: JoinTableRequest): Promise<Response> {
    return new Promise((resolve, reject) => {
      this.socket.emit(SocketEvent.JOIN_TABLE, req, (res: Response) => {
        resolve(res);
      });
    });
  }

  sendChat(req: ChatRequest): Promise<Response> {
    return new Promise((resolve, reject) => {
      this.socket.emit(SocketEvent.CHAT, req, (res: Response) => {
        resolve(res);
      });
    });
  }

  sendStateUpdate(req: UpdateViewRequest): Promise<Response> {
    return new Promise((resolve, reject) => {
      this.socket.emit(SocketEvent.UPDATE_VIEW, req, (res: Response) => resolve(res));
    });
  }

  sendRegisterViewer(req: ViewTableRequest): Promise<Response> {
    return new Promise((resolve, reject) => {
      this.socket.emit(SocketEvent.VIEW_TABLE, req, (res: Response) => resolve(res));
    });
  }

  sendPlayerAction(req: PlayerActionRequest): Promise<Response> {
    return new Promise((resolve, reject) => {
      this.socket.emit(SocketEvent.PLAYER_ACTION, req, (res: Response) => resolve(res));
    });
  }

  sendUpdatePlayerState(req: UpdatePlayerStateRequest): Promise<Response> {
    return new Promise((resolve, reject) => {
      this.socket.emit(SocketEvent.UPDATE_PLAYER_STATE, req, (res: Response) => resolve(res));
    });
  }

  toString(): string {
    return `Connection{id=${this.socket.id} uuid=${this.uuid}}`;
  }

  private onDisconnect = () => {
    this.connectionManager.destroyConnection(this.socket.id);
    this.pokerServer.logState();
  }

  private handleSetUuid = async (req: Request): Promise<Response> => {
    const uuid = req.uuid as string;
    if (this.connectionManager.setUuid(uuid, this)) {
      this.uuid = uuid;
      return SUCCESS;
    }
    return INVALID_REQUEST;
  }

  private handleHostTable = async (req: HostTableRequest): Promise<Response> => {
    const tableId = req.tableId;
    const result = this.tableManager.hostTable(tableId, this);
    return { result };
  }

  private handleJoinTable = async (req: JoinTableRequest): Promise<Response> => {
    return this.tableManager.joinTable(req);
  }

  private handleChat = async (req: ChatRequest): Promise<Response> => {
    return this.tableManager.chat(req);
  }

  private handleViewTable = (req: ViewTableRequest): Promise<Response> => {
    return Promise.resolve(this.tableManager.registerViewer(req));
  }

  private handleUpdateView = (req: UpdateViewRequest): Promise<Response> => {
    return Promise.resolve(this.tableManager.updateViewers(req));
  }

  private handlePlayerAction = (req: PlayerActionRequest): Promise<Response> => {
    return this.tableManager.forwardPlayerAction(req);
  }

  private handleUpdatePlayerState = (req: UpdatePlayerStateRequest): Promise<Response> => {
    return this.tableManager.updatePlayerState(req);
  }

  private registerRequestHandler
    <T extends Request, R extends Response>(event: SocketEvent, handler: RequestHandler<T, R>): void {
    const safeHandler = async (req: T, callback: Callback<R>) => {
      logger.debug(`Received request from ${this}: ${JSON.stringify(req)}`);
      const safeCallback = (callback && typeof callback === 'function')
        ? callback
        : (res: R) => logger.warn(`No callback supplied `);

      if (!req) {
        logger.warn(`Request is empty`);
        safeCallback(INVALID_REQUEST as R);
        return;
      }

      if (event !== SocketEvent.SET_UUID) {
        if (!req.uuid || req.uuid !== this.uuid) {
          logger.warn(`Missing or invalid UUID`);
          safeCallback(INVALID_REQUEST as R);
          return;
        }
      }

      // TODO: validate request args
      try {
        const response = await this.timeoutHandler(handler)(req);
        safeCallback(response);
      } catch (err) {
        logger.error(`Error while handling request: ${err}`);
        safeCallback(INVALID_REQUEST as R);
      }
      this.pokerServer.logState();
    };
    this.socket.on(event.toLowerCase(), safeHandler);
  }

  private timeoutHandler<T extends Request, R extends Response>(handler: RequestHandler<T, R>): RequestHandler<T, R> {
    return async (req: T) => {
      const timeout = 10000;
      let timedOut = false;
      let handled = false;

      return new Promise<R>(async (resolve, reject) => {
        setTimeout(
          () => {
            if (!handled) {
              timedOut = true;
              logger.warn(`Request timed out`);
              timedOut = true;
              resolve(TIMEOUT as R);
            }
          },
          timeout);

        const res = await handler(req);
        handled = true;
        if (timedOut) {
          resolve(TIMEOUT as R);
        } else {
          resolve(res);
        }
      });
    };
  }
}