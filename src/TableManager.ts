import { ChatRequest, JoinTableRequest, PlayerActionRequest, UpdatePlayerStateRequest, UpdateViewRequest, ViewTableRequest } from '../client/src/common/socket/Request';
import { INVALID_REQUEST, INVALID_TABLE, Response, Result } from '../client/src/common/socket/Response';

import { Connection } from './Connection';
import { ConnectionManager } from './ConnectionManager';
import { Table } from './Table';
import { logger } from './Logger';

export interface TableManager {

  hostTable(tableId: string, connection: Connection): Result;
  joinTable(req: JoinTableRequest): Promise<Response>;
  chat(req: ChatRequest): Promise<Response>;
  tableCount(): number;
  registerViewer(req: ViewTableRequest): Promise<Response>;
  updateViewers(req: UpdateViewRequest): Response;
  forwardPlayerAction(req: PlayerActionRequest): Promise<Response>;
  updatePlayerState(req: UpdatePlayerStateRequest): Promise<Response>;
}

export class TableManagerImpl implements TableManager {

  connectionManager: ConnectionManager;

  private readonly tablesById: Map<string, Table> = new Map();
  private readonly tablesByHost: Map<string, string> = new Map();

  tableCount() {
    return this.tablesById.size;
  }

  async joinTable(req: JoinTableRequest): Promise<Response> {
    const tableId = req.tableId;
    const table = this.tablesById.get(tableId);
    if (!table) {
      logger.warn(`No table found for ${tableId}`);
      return INVALID_TABLE;
    }

    return table.joinTable(req);
  }

  hostTable(tableId: string, connection: Connection): Result {
    if (!connection.uuid) {
      return Result.INVALID_TABLE;
    }

    const existingTable = this.tablesById.get(tableId);
    const existingTableForHost = this.tablesByHost.get(connection.uuid);

    if (existingTable) {
      if (existingTable.host !== connection.uuid) {
        logger.warn(`Table ${tableId} is already being hosted by ${existingTable.host}`);
        return Result.INVALID_TABLE;
      } else {
        logger.info(`${connection} is already hosting table ${tableId}`);
        return Result.SUCCESS;
      }
    }

    if (existingTableForHost) {
      if (existingTableForHost === tableId) {
        logger.info(`${connection} is already hosting table ${tableId}`);
        return Result.SUCCESS;
      } else {
        logger.warn(`${connection} is currently hosting table ${existingTableForHost}. Terminating`);
        this.tablesById.delete(existingTableForHost);
        this.tablesByHost.delete(connection.uuid);
        // End table
      }
    }

    logger.info(`${connection.uuid}: Now hosting table ${tableId}`);
    const table = new Table(tableId, connection.uuid, this.connectionManager);
    this.tablesById.set(tableId, table);
    this.tablesByHost.set(connection.uuid, tableId);
    return Result.SUCCESS;
  }

  chat = async (req: ChatRequest): Promise<Response> => {
    const tableId = req.tableId;
    const table = this.tablesById.get(tableId);
    if (!table) {
      logger.warn(`No table found for ${tableId}`);
      return INVALID_TABLE;
    }

    return table.sendChat(req);
  }

  async registerViewer(req: ViewTableRequest): Promise<Response> {
    const tableId = req.tableId;
    const viewerId = req.uuid;

    if (!tableId || !viewerId) {
      logger.warn(`Missing table id or viewer id`);
      return INVALID_REQUEST;      
    }

    const table = this.tablesById.get(tableId);
    if (!table) {
      logger.warn(`No table found for ${tableId}`);
      return INVALID_TABLE;
    }

    return table.registerViewer(req);
  }

  updateViewers(req: UpdateViewRequest): Response {
    const tableId = req.tableId;

    if (!tableId) {
      logger.warn(`Missing table id`);
      return INVALID_REQUEST;      
    }

    const table = this.tablesById.get(tableId);
    if (!table) {
      logger.warn(`No table found for ${tableId}`);
      return INVALID_TABLE;
    }

    return table.updateViewers(req);
  }

  async forwardPlayerAction(req: PlayerActionRequest): Promise<Response> {
    const tableId = req.tableId;

    if (!tableId) {
      logger.warn(`Missing table id`);
      return INVALID_REQUEST;      
    }

    const table = this.tablesById.get(tableId);
    if (!table) {
      logger.warn(`No table found for ${tableId}`);
      return INVALID_TABLE;
    }

    return table.forwardPlayerAction(req);
  }

  async updatePlayerState(req: UpdatePlayerStateRequest): Promise<Response> {
    const tableId = req.tableId;

    if (!tableId) {
      logger.warn(`Missing table id`);
      return INVALID_REQUEST;      
    }

    const table = this.tablesById.get(tableId);
    if (!table) {
      logger.warn(`No table found for ${tableId}`);
      return INVALID_TABLE;
    }

    return table.updatePlayerState(req);
  }
}