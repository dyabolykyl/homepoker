import * as http from 'http';
import * as io from 'socket.io';

import { ConnectionManager, ConnectionManagerImpl } from './ConnectionManager';
import { TableManager, TableManagerImpl } from './TableManager';

import { logger } from './Logger';

export class PokerServer {
  private readonly connectionManager: ConnectionManager;
  private readonly tableManager: TableManager;

  private server: io.Server;

  constructor() {
    this.tableManager = new TableManagerImpl();
    this.connectionManager = new ConnectionManagerImpl(this.tableManager, this);
    (this.tableManager as TableManagerImpl).connectionManager = this.connectionManager;
  }

  start(server: http.Server) {
    logger.info(`Hosting poker server at ${JSON.stringify(server.address())}`);
    this.server = io(server);
    this.server.on('connection', this.onConnection);
    this.logState();
  }

  logState() {
    logger.debug(`Server State: ${this.connectionManager.connectionCount()} connections, ${this.tableManager.tableCount()} Tables\n`);
  }

  private onConnection = (socket: io.Socket) => {
    logger.info(`New connection: ${socket.id}`);
    this.connectionManager.createConnection(socket);
    this.logState();
  }

}
