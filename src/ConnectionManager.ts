import { Connection } from './Connection';
import { PokerServer } from './PokerServer';
import { TableManager } from './TableManager';
import { logger } from './Logger';

export interface ConnectionManager {

  createConnection(socket: SocketIO.Socket): void;
  destroyConnection(id: string): void;
  connectionCount(): number;
  getConnection(uuid: string): Connection | undefined;
  setUuid(uuid: string, connection: Connection): boolean;

}

export class ConnectionManagerImpl implements ConnectionManager {
  private readonly tableManager: TableManager;
  private readonly pokerServer: PokerServer;

  private connectionsBySocket: Map<string, Connection> = new Map();
  private connectionsByUuid: Map<string, Connection> = new Map();

  constructor(tableManager: TableManager, gameServer: PokerServer) {
    this.tableManager = tableManager;
    this.pokerServer = gameServer;
  }

  createConnection(socket: SocketIO.Socket): void {
    const connection = new Connection(socket, this.tableManager, this, this.pokerServer);
    this.connectionsBySocket.set(socket.id, connection);
  }

  destroyConnection(id: string) {
    logger.info(`Destroying connection: ${id}`);
    const connection = this.connectionsBySocket.get(id);
    if (!connection) {
      return;
    }

    this.connectionsBySocket.delete(id);
    if (!connection.uuid) {
      return;
    }

    const current = this.connectionsByUuid.get(connection.uuid);
    if (current && current.socket.id === id) {
      logger.info(`Unregistering uuid for ${connection}`);
      this.connectionsByUuid.delete(connection.uuid);
    }
  }

  connectionCount(): number {
    return this.connectionsBySocket.size;
  }

  getConnection(uuid: string) {
    return this.connectionsByUuid.get(uuid);
  }

  setUuid(uuid: string, connection: Connection): boolean {
    const existingConnection = this.connectionsByUuid.get(uuid);
    if (!existingConnection) {
      logger.info(`${uuid}: Setting socket to ${connection}`);
      this.connectionsByUuid.set(uuid, connection);
      return true;
    }

    if (existingConnection.socket.id === connection.socket.id) {
      logger.info(`${uuid}: Duplicate SET_UUID request, ignoring`);
      return true;
    }

    logger.info(`${uuid}: Found existing connection ${existingConnection}. Replacing with ${connection}`);
    this.connectionsByUuid.set(uuid, connection);
    existingConnection.close();
    return true;
  }
}