import { Request, SocketEvent, makeSetUuidRequest } from '../common/socket/Request';
import { Response, Result } from '../common/socket/Response';

import { Emitter } from '../common/utils/Emitter';
import { Lifecycle } from '../common/utils/Lifecycle';
import { RequestHandler } from './SocketAdapter';
import { SocketAdapter } from '../socket/SocketAdapter';
import { log } from '../utils/log';

export interface SessionManager extends Lifecycle {

  getUuid(): string;
  send<T extends Response>(request: Request): Promise<T>;
  onConnect(handler: () => void): void;
  listen<R extends Request, T extends Response>(event: SocketEvent, handler: RequestHandler<R, T>): void;
}

export class SessionManagerImpl extends Emitter implements SessionManager {
  private readonly socketAdapter: SocketAdapter;
  private readonly uuid: string;

  constructor(socketAdapter: SocketAdapter) {
    super();
    this.socketAdapter = socketAdapter;
    this.uuid = guid();
  }

  getUuid(): string {
    return this.uuid;
  }

  start() {
    this.socketAdapter.onConnect(this.onConnected);
  }

  stop(): void {
    this.socketAdapter.disconnect();
  }

  onConnect(handler: () => void) {
    this.on('connect', handler);
  }

  send<T extends Response>(req: Request): Promise<T> {
    req.uuid = this.uuid;
    return this.socketAdapter.send(req) as Promise<T>;
  }

  listen<R extends Request, T extends Response>(event: SocketEvent, handler: RequestHandler<R, T>) {
    this.socketAdapter.listen(event, handler);
  }

  private onConnected = async (): Promise<void> => {
    log.info(`Connection established. Attempting to set UUID: ${this.uuid}`);

    const request = makeSetUuidRequest(this.uuid);
    try {
      const res: Response = await this.socketAdapter.send(request);
      if (res.result === Result.CONNECTION_ERROR) {
        log.warn(`Failed to set UUID. Connection error`);
        return;
      }
      
      log.info(`UUID successfully set`);
      this.emit('connect');
    } catch (err) {
      log.warn(`Error setting UUID: ${err}`);
    }
  }
}

function guid(): string {
  return s4();
  // + s4() + '-' + s4() + '-' + s4() + '-' +
  //  s4() + '-' + s4() + s4() + s4();
}

function s4(): string {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}
