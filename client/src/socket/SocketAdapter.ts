import * as io from 'socket.io-client';

import {
  CONNECTION_ERROR,
  Callback,
  INVALID_REQUEST,
  Response,
} from '../common/socket/Response';
import { Request, SocketEvent } from '../common/socket/Request';

import { Emitter } from '../common/utils/Emitter';
import { FakeGameServer } from '../host/fakes/FakeGameServer';
import { Lifecycle } from '../common/utils/Lifecycle';
import { log } from '../utils/log';

export type RequestHandler<R extends Request, T extends Response> = (req: R) => T;

export interface SocketAdapter {

  disconnect(): void;

  send<T extends Response>(request: Request): Promise<T | Response>;

  listen<R extends Request, T extends Response>(event: SocketEvent, callback: RequestHandler<R, T>): void;

  onConnect(callback: () => void): void;

  onDisconnect(callback: () => void): void;

  isConnected(): boolean;

}

export class FakeSocketAdapter extends Emitter implements SocketAdapter, Lifecycle {

  private fakeGameServer: FakeGameServer;
  private connected = false;

  constructor(fakeGameServer: FakeGameServer) {
    super();
    this.fakeGameServer = fakeGameServer;
  }
  
  start() {
    this.connect();
  }

  stop() {
    this.disconnect();
  }

  onConnect(callback: () => void): void {
    log.debug(`Registering onConnect`);
    this.on('connect', callback);
    if (this.connected) {
      callback();
    }
  }

  onDisconnect(callback: () => void): void {
    
  }

  isConnected(): boolean {
    return this.connected;
  }

  connect(): void {
    log.debug(`Connecting`);
    this.connected = true;
    this.emit('connect');
    return;
  }

  disconnect(): void {
    this.connected = false;
    return;
  }

  send<T extends Response>(request: Request): Promise<T | Response> {
    return Promise.resolve(this.fakeGameServer.send(request));
  }

  listen<R extends Request, T extends Response>(event: SocketEvent, callback: RequestHandler<R, T>): void {
    this.fakeGameServer.listen(event, (req: R) => {
      log.debug(`Request received: ${JSON.stringify(req)}`);
      return callback(req);
    });
  }
}

export class SocketIoSocketAdapter extends Emitter implements SocketAdapter {
  // private endpoint: string = 'http://192.168.1.107:8080';
  private endpoint: string = `http://192.168.1.107:5000`;
  private server: any;

  constructor() {
    super();
    log.info(`Connecting to ${this.endpoint}`);
    this.server = io.connect();
    this.server.on('connect', this.onConnection);
    this.server.on('connect_error', this.onConnectError);
    this.server.on('disconnect', this.onDisconnection);
  }

  disconnect(): void {
    if (this.server) {
      this.server.disconnect();
    }
  }

  isConnected() {
    return this.server.connected;
  }

  send(request: Request): Promise<Response> {
    return new Promise((resolve, reject) => {
      // if (!this.isConnected()) {
      //   resolve({ result: Result.CONNECTION_ERROR });
      // }

      const event = request.event.toLowerCase();
      log.debug(`Emitting ${event} event with ${JSON.stringify(request)}`);
      this.server.emit(request.event.toLowerCase(), request, (res: Response) => {
        log.debug(`Response received: ${JSON.stringify(res)}`);
        if (!res || !res.result) {
          log.warn(`Response missing`);
          resolve(INVALID_REQUEST);
        }
        resolve(res);
      });
    });
  }

  listen = <R extends Request, T extends Response> (event: SocketEvent, requestHandler: RequestHandler<R, T>) => {
    return this.server.on(event, (req: R, callback: Callback<T>) => {
      log.debug(`Received request ${JSON.stringify(req)}`);

      const safeCallback = callback || ((r) => {
        log.warn(`Callback was missing`);
      });

      try {
        if (!req) {
          log.warn(`Missing request`);
          safeCallback(CONNECTION_ERROR as T);
          return;
        }

        const res: T = requestHandler(req);
        safeCallback(res);
      } catch (err) {
        log.error(`Error while serving request: ${err}`);
        safeCallback(INVALID_REQUEST as T);
      }
    });
  }

  onConnect(callback: () => void) {
    this.on('connect', callback);
    if (this.isConnected()) {
      callback();
    }
  }

  onDisconnect(callback: () => void) {
    this.on('disconnect', callback);
  }

  private onConnection = (): void => {
    log.info(`Connected to ${this.endpoint}`);
    this.emit('connect');
  }

  private onConnectError = (): void => {
    log.warn(`Failed to connect to ${this.endpoint}`);
  }

  private onDisconnection = (): void => {
    log.warn(`Socket disconnected`);
    this.emit('disconnect');
  }
}
