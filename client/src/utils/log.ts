
let shouldLog = true;

export let log: Console = console;

export const emptyLog = {
    debug(message?: any, ...optionalParams: any[]) { },
    error(message?: any, ...optionalParams: any[]) { },
    info(message?: any, ...optionalParams: any[]) { },
    log(message?: any, ...optionalParams: any[]) { },
    time(label: string) { },
    timeEnd(label: string) { },
    trace(message?: any, ...optionalParams: any[]) { },
    warn(message?: any, ...optionalParams: any[]) { },
};
