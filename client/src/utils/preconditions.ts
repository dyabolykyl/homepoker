
export function checkNotNull<T>(obj: T, msg: string): T {
  if (!obj) {
    throw new Error(`Object is null: ${msg}`);
  }
  return obj;
}

export function checkState(condition: boolean, msg: string): void {
  if (!condition) {
    throw new Error(msg);
  }
}
