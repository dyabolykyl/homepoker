import { Lifecycle } from '../common/utils/Lifecycle';
import { log } from './log';

export class Context implements Lifecycle {

  private components: Map<string, any> = new Map();

  addComponent<T>(name: string, component: T): T {
    this.components.set(name, component);
    return component;
  }

  getComponent<T>(name: string): T {
    return this.components.get(name) as T;
  }

  start() {
    log.info(`Starting components`);
    this.components.forEach((v, k) => {
      if (v.start) {
        v.start();
      }
    });
  }

  stop() {
    this.components.forEach((v, k) => {
      if (v.stop) {
        v.stop();
      }
    });
  }

}