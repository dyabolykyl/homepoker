import { EventBus, EventType } from '../../common/utils/Emitter';
import { JoinTableRequest, PlayerActionRequest, Request, SocketEvent, UpdatePlayerStateRequest } from '../../common/socket/Request';
import { Response, SUCCESS } from '../../common/socket/Response';
import { Scenario, ShowdownScenario } from './Scenario';

import { ActionType } from '../../common/game/PlayerAction';
import { Card } from '../../common/models/Card';
import { EMPTY_POT } from '../../common/game/PotState';
import { FullPlayerState } from '../../common/game/TableState';
import { HandPlayerStatus } from '../../common/models/HandPlayer';
import { HandState } from '../../common/game/HandState';
import { Lifecycle } from '../../common/utils/Lifecycle';
import { PlayerHand } from '../../common/models/PlayerHand';
import { RequestHandler } from '../../socket/SocketAdapter';
// import { SimpleFoldScenario } from './Scenario';
import { Suit } from '../../common/models/Suit';
import { TableIdSetEvent } from '../game/TableRunner';
import { Value } from '../../common/models/Value';
import { log } from '../../utils/log';

export interface FakePlayer {
  name: string;
  uuid: string;
  actualUuid?: string;
}

export interface PlayerActionSender {
  sendPlayerAction(name: string, action: ActionType, amount?: number): void;
}

export class FakeGameServer implements Lifecycle, PlayerActionSender {

  private readonly eventBus: EventBus;
  private readonly fakePlayers: FakePlayer[];

  private joinGameHandler: RequestHandler<JoinTableRequest, Response>;
  // private chatHandler: RequestHandler<ChatRequest, Response>;
  private playerActionHandler: RequestHandler<PlayerActionRequest, Response>;
  private updatePlaterStateHandler: RequestHandler<UpdatePlayerStateRequest, Response>;

  private tableId: string;
  private scenario: Scenario = new ShowdownScenario(this);

  constructor(eventBus: EventBus) {
    this.eventBus = eventBus;
    this.fakePlayers = [];
  }

  start() {
    this.eventBus.subscribe(EventType.HOST_TABLE_ID_SET, this.addStaticPlayers);
    this.eventBus.subscribe(EventType.GAME_STARTED, this.sendActions);
  }

  stop() {

  }

  send<T extends Response>(request: Request): T | Response {
    log.debug(`Responding to ${JSON.stringify(request)}`);
    if (request.event === SocketEvent.JOIN_TABLE) {
      this.fakePlayers[0].uuid = request.uuid as string;
      this.sendStateUpdates();
    }
    return SUCCESS;
  }

  listen<R extends Request, T extends Response>(event: SocketEvent, callback: RequestHandler<R, T>): void {
    log.debug(`Intercepting ${event}`);
    switch (event) {
      case SocketEvent.JOIN_TABLE:
        this.joinGameHandler = callback as any;
        break;
      case SocketEvent.CHAT:
        // this.chatHandler = callback as any;
        break;
      case SocketEvent.PLAYER_ACTION:
        this.playerActionHandler = callback as any;
        break;
      case SocketEvent.UPDATE_PLAYER_STATE:
        this.updatePlaterStateHandler = callback as any;
        break;
      default:
        log.debug(`${event} not handled by fake server`);
    }
  }

  sendPlayerAction(name: string, action: ActionType, amount?: number) {
    const player = this.fakePlayers.find(p => p.name === name);
    if (!player) {
      return;
    }
    const uuid = player.uuid;
    let playerActionRequest = new PlayerActionRequest(this.tableId, {
      id: uuid,
      actionType: action,
      amount
    });
    playerActionRequest.uuid = uuid;
    this.playerActionHandler(playerActionRequest);
  }

  private arrangeFakePlayer = (name: string) => {
    const fakePlayer = {
      name,
      uuid: 'uuid-' + name,
    };
    this.fakePlayers.push(fakePlayer);
    this.joinGameHandler({
      event: SocketEvent.JOIN_TABLE,
      uuid: fakePlayer.uuid,
      playerName: fakePlayer.name,
      tableId: this.tableId
    });
  }

  private addStaticPlayers = (event: TableIdSetEvent): void => {
    this.tableId = event.tableId;
    if (!this.scenario) {
      return;
    }

    log.debug(`Adding static players`);
    this.scenario.getFakePlayers().forEach(this.arrangeFakePlayer);

    // const chatRequest = new ChatRequest(event.tableId, 'Hello');
    // chatRequest.uuid = 'uuid1';
    // this.chatHandler(chatRequest);
  }

  private sendActions = () => {
    if (!this.scenario) {
      return;
    }
    log.debug(`Sending actions`);
    this.scenario.runActions();
  }
  
  private arrangePlayerState(active: boolean): void {
    log.debug(`Arranging player update with active=${active}`);
    const handState: HandState = {
      board: [],
      handPlayers: [ ],
      cardsFaceUp: false,
      potState: EMPTY_POT,
      bettingState: {
        bettingPlayerStates: []
      },
      handId: 1
    };

    const state: FullPlayerState = {
      playerState: {
        id: this.fakePlayers[0].uuid,
        stack: 100,
        name: this.fakePlayers[0].name,
        isHost: true,
        seat: 4,
      },
      handState,
      handPlayerState: {
        id: this.fakePlayers[0].uuid,
        playerHand: new PlayerHand([new Card(Suit.CLUB, Value.ACE), new Card(Suit.DIAMOND, Value.KING)]),
        status: HandPlayerStatus.IN,
      }
    };
    const req = new UpdatePlayerStateRequest(this.tableId, state);
    req.uuid = state.playerState.id;
    this.updatePlaterStateHandler(req);
  }

  private sendStateUpdates() {
    setTimeout(() => this.arrangePlayerState(false), 1000);
    setTimeout(() => this.arrangePlayerState(true), 2000);
    setTimeout(() => this.arrangePlayerState(false), 10000);
  }
}
