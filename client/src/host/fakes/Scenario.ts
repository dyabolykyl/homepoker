import { ActionType } from '../../common/game/PlayerAction';
import { PlayerActionSender } from './FakeGameServer';

export interface Scenario {

  getFakePlayers(): string[];
  runActions(): void;

}

abstract class AbstractScenario implements Scenario {

  private readonly actionSender: PlayerActionSender;
  private readonly numPlayers: number;
  private time: number = 500;

  constructor(actionSender: PlayerActionSender, numPlayers: number) {
    this.actionSender = actionSender;
    this.numPlayers = numPlayers;
  }

  getFakePlayers(): string[] {
    const players = [];
    for (let i = 1; i <= this.numPlayers; i++) {
      players.push('p' + i);
    }
    return players;
  }

  abstract runActions(): void;

  protected sheduleAction(player: number, action: ActionType, amount?: number) {
    setTimeout(() => this.actionSender.sendPlayerAction('p' + player, action, amount), this.time);
    this.time += 500;
  }
}

export class FullTableScenario extends AbstractScenario {

  constructor(actionSender: PlayerActionSender) {
    super(actionSender, 8);
  }

  runActions() {

  }
}

export class SimpleFoldScenario extends AbstractScenario {

  constructor(actionSender: PlayerActionSender) {
    super(actionSender, 2);
  }

  runActions() {
    this.sheduleAction(1, ActionType.FOLD);
    this.sheduleAction(2, ActionType.FOLD);
  }
}

export class AllInScenario extends AbstractScenario {

  constructor(actionSender: PlayerActionSender) {
    super(actionSender, 3);
  }

  runActions() {
    this.sheduleAction(3, ActionType.BET, 100);
    this.sheduleAction(2, ActionType.CALL);
    this.sheduleAction(1, ActionType.CALL);
  }
}

export class ShowdownScenario extends AbstractScenario {

  constructor(actionSender: PlayerActionSender) {
    super(actionSender, 2);
  }

  runActions() {
    this.sheduleAction(2, ActionType.CALL);
    this.sheduleAction(1, ActionType.CHECK);

    this.sheduleAction(1, ActionType.CHECK);
    this.sheduleAction(2, ActionType.CHECK);

    this.sheduleAction(1, ActionType.CHECK);
    this.sheduleAction(2, ActionType.CHECK);

    this.sheduleAction(1, ActionType.CHECK);
    this.sheduleAction(2, ActionType.CHECK);
  }
}