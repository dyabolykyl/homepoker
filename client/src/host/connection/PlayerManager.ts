import { ChatRequest, JoinTableRequest, SocketEvent } from '../../common/socket/Request';
import { INVALID_REQUEST, INVALID_TABLE, JoinTableResponse, Response, SUCCESS } from '../../common/socket/Response';

import { Lifecycle } from '../../common/utils/Lifecycle';
import { Player } from '../../common/models/Player';
import { SessionManager } from '../../socket/SessionManager';
import { TableRunner } from '../game/TableRunner';
import { TableSeater } from '../game/TableSeater';
import { TableStateSupplier } from '../game/TableStateSupplier';
import { checkNotNull } from '../../utils/preconditions';
import { log } from '../../utils/log';

export interface PlayerManager {

  setTableRunner(tableRunner: TableRunner): void;

}

export class PlayerManagerImpl implements PlayerManager, Lifecycle {
  private readonly sessionManager: SessionManager;
  private readonly tableSeater: TableSeater;
  private readonly tableStateSupplier: TableStateSupplier;
  private tableRunner: TableRunner;

  constructor(sessionManager: SessionManager, tableStateSupplier: TableStateSupplier) {
    this.sessionManager = sessionManager;
    this.tableSeater = new TableSeater();
    this.tableStateSupplier = tableStateSupplier;
  }

  setTableRunner(tableRunner: TableRunner) {
    this.tableRunner = tableRunner;
  }

  start() {
    checkNotNull(this.tableRunner, 'tableRunner');
    this.sessionManager.listen(SocketEvent.JOIN_TABLE, this.onJoinGame);
    this.sessionManager.listen(SocketEvent.CHAT, this.onChat);
  }

  stop() {

  }

  private onJoinGame = (req: JoinTableRequest): JoinTableResponse => {
    log.debug(`Join game attempt from: ${JSON.stringify(req)}`);
    
    const tableState = this.tableStateSupplier.getTableState();
    if (tableState.tableId !== req.tableId) {
      log.warn(`Invalid table id`);
      return INVALID_TABLE as JoinTableResponse;
    }

    if (tableState.getPlayer(req.uuid)) {
      log.info(`${req.uuid} already in game`);
      const state = tableState.getFullPlayerState(req.uuid);
      return new JoinTableResponse(state);
    }

    if (this.tableSeater.isFull()) {
      log.warn(`No more seats at table`);
      return INVALID_REQUEST as JoinTableResponse;
    }

    const seat = this.tableSeater.getNextSeat();
    const player = new Player(req.uuid as string, req.playerName, seat, 100); 

    tableState.addPlayerState(player.state);
    if (tableState.playerStates.length === 1) {
      tableState.host = player.state.id;
      player.state.isHost = true;
    }

    this.tableRunner.onPlayerJoined(player);
    return new JoinTableResponse(tableState.getFullPlayerState(player.state.id));
  }

  private onChat = (req: ChatRequest): Response => {
    log.debug(`Chat request: ${JSON.stringify(req)}`);
    
    if (this.tableStateSupplier.getTableState().tableId !== req.tableId) {
      log.warn(`Invalid table id`);
      return INVALID_TABLE;
    }

    const playerState = this.tableStateSupplier.getTableState().getPlayer(req.uuid);
    if (!playerState) {
      log.warn(`${req.uuid} not in game`);
      return INVALID_TABLE;
    }

    if (!req.message) {
      log.warn(`Request missing a message`);
      return INVALID_REQUEST;
    }

    this.tableRunner.onChat(playerState.id, req.message);
    return SUCCESS;
  }
}
