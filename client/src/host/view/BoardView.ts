import * as Assets from '../../assets';

import { Card } from '../../common/models/Card';
import { GameState } from '../../common/game/GameState';
import { TableStateSupplier } from '../game/TableStateSupplier';
import { getFrame } from './cards';
import { log } from '../../utils/log';

const CARD_SCALE = .6;
const CARD_OFFSET_X = 10;

export class BoardView {

  private readonly cards: Phaser.Sprite[];
  private readonly game: Phaser.Game;
  private readonly tableStateSupplier: TableStateSupplier;

  private potText: Phaser.Text;

  constructor(game: Phaser.Game, tableStateSupplier: TableStateSupplier) {
    this.game = game;
    this.cards = [];
    this.tableStateSupplier = tableStateSupplier;
  }

  clearBoard = (): void => {
    this.cards.forEach(card => card.destroy());
    this.cards.splice(0);
  }

  update = (): void => {
    log.info(`Updating board view`);
    this.clearBoard();
    const board = this.getGameState().handState.board;

    for (let i = 0; i < 5; i++) {
      if (board.length <= i) {
        break;
      }

      const card: Card = board[i];
      const frame = getFrame(card);
      const cardView = this.game.add.sprite(0, 0, Assets.Spritesheets.SpritesheetsCards167220.getName(), frame);
      cardView.anchor.setTo(0.5);
      cardView.scale.setTo(CARD_SCALE);
      cardView.position.setTo(this.game.world.centerX + (i - 2) * cardView.width + (i - 2) * CARD_OFFSET_X, this.game.world.centerY);
      log.debug(`Displaying ${card} in position ${i} at ${cardView.x}, ${cardView.y}`);
      this.cards.push(cardView);
    }

    this.updatePotText();
  }

  private updatePotText() {
    if (!this.potText) {
      this.potText = this.game.add.text(this.game.world.centerX, this.game.world.centerY - 100, '', {
        font: '40px Arial',
        fill: 'white'
      });
      this.potText.anchor.set(0.5);
      log.debug(`Created pot text at ${this.potText.x}, ${this.potText.y}`);
    }

    const gameState = this.getGameState();
    if (gameState.running) {
      this.potText.text = 'Pot: ' + gameState.handState.potState.subPots.join(', ');
    } else {
      this.potText.text = '';
    }
  }

  private getGameState(): GameState {
    return this.tableStateSupplier.getTableState().gameState;
  }
}
