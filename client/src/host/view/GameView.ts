
export interface GameView {

  addPlayer(playerId: string): void;

  displayChatMessage(playerId: string): void;

  enableGameViewing(tableId: string): void;

  updateView(): void;
}