import * as Assets from '../../assets';

import { HandPlayerState, HandPlayerStatus } from '../../common/models/HandPlayer';

import { BettingPlayerState } from '../../common/game/BettingState';
import { Card } from '../../common/models/Card';
import { HandState } from '../../common/game/HandState';
import { PlayerState } from '../../common/game/PlayerState';
import { TableStateSupplier } from '../game/TableStateSupplier';
import { checkNotNull } from '../../utils/preconditions';
import { getFrame } from './cards';
import { log } from '../../utils/log';

const PLAYER_FONT = '30px Arial';
const BET_FONT = '20px Arial';

export class PlayerView {
  readonly playerId: string;
  private readonly playerHolder: Phaser.Group;
  private readonly game: Phaser.Game;
  private readonly tableStateSupplier: TableStateSupplier;

  private backgroundBorder: Phaser.Image;
  private backgroundImage: Phaser.Image;
  private nameText: Phaser.Text;
  private stackText: Phaser.Text;
  private chatText: Phaser.Text;
  private cardOneImage: Phaser.Image | Phaser.Sprite;
  private cardTwoImage: Phaser.Image | Phaser.Sprite;
  private betText: Phaser.Text;

  constructor(playerId: string, playerHolder: Phaser.Group, x: number, y: number, game: Phaser.Game, tableStateSupplier: TableStateSupplier) {
    this.playerId = checkNotNull(playerId, 'playerId');
    this.playerHolder = checkNotNull(playerHolder, 'playerHolder');
    this.game = checkNotNull(game, 'game');
    this.tableStateSupplier = tableStateSupplier;

    this.setBackground(x, y);
    this.setName();
    this.setStack();
    this.createChatText();
    this.createBetText();
  }

  destroy() {
    log.info(`Destroying player view for ${this.playerId}`);
    this.playerHolder.destroy();
  }

  update() {
    // log.debug(`Updating player view for ${this.playerId}`);
    this.updateName();
    this.updateStack();
    this.updateChatMessage();
    this.updateCards();
    this.updateBet();
    this.updateStatus();
  }

  setBackground(x: number, y: number) {
    this.backgroundBorder = this.game.add.image(x, y, Assets.Images.ImagesActivePlayerBorder.getName());
    this.backgroundBorder.anchor.setTo(0.5);
    this.backgroundBorder.visible = false;
    this.playerHolder.add(this.backgroundBorder);
    
    this.backgroundImage = this.game.add.image(x, y, Assets.Images.ImagesPlayerHolder.getName());
    this.backgroundImage.anchor.setTo(0.5);
    this.playerHolder.add(this.backgroundImage);
  }

  setName() {
    const playerState = this.getPlayerState();
    if (!playerState) {
      return;
    }
    const x = -this.backgroundImage.width / 2 + 5;
    const y = -this.backgroundImage.height / 2 + 5;
    this.nameText = this.game.add.text(x, y);
    this.nameText.setStyle({
      font: PLAYER_FONT
    });
    this.backgroundImage.addChild(this.nameText);
    this.updateName();
  }

  updateName() {
    const playerState = this.getPlayerState();
    if (!playerState) {
      return;
    }
    this.nameText.setText(playerState.name);
  }

  setStack() {
    const playerState = this.getPlayerState();
    if (!playerState) {
      return;
    }
    const x = -this.backgroundImage.width / 2 + 5;
    const y = 5;
    this.stackText = this.game.add.text(x, y);
    this.stackText.setStyle({
      font: PLAYER_FONT
    });
    this.backgroundImage.addChild(this.stackText);
    this.updateStack();
  }

  updateStack() {
    const playerState = this.getPlayerState();
    if (!playerState) {
      return;
    }
    this.stackText.setText(playerState.stack + '');
  }

  updateChatMessage() {
    const playerState = this.getPlayerState();
    if (!playerState) {
      return;
    }
    this.chatText.setText(playerState.chatMessage || '');
  }

  updateCards() {
    this.clearCards();
    const handPlayer = this.getHandPlayer();
    if (!handPlayer) {
      return;
    }

    const cardOne = handPlayer.playerHand.cards[0];
    const cardTwo = handPlayer.playerHand.cards[1];

    let cardImage = this.displayCard(cardOne, 0);
    if (cardImage) {
      this.cardOneImage = cardImage;
      this.cardOneImage.position.set(this.cardOneImage.width / 2, this.cardOneImage.y);
    }

    cardImage = this.displayCard(cardTwo, this.cardOneImage.width * 1.1);
    if (cardImage) {
      this.cardTwoImage = cardImage;
    }
  }

  private updateBet() {
    let amount = 0;
    const bettingState = this.getBettingPlayerState();
    if (bettingState) {
      amount = bettingState.activeBet;
    }

    // log.debug(`Setting bet text to ${amount}`);
    this.betText.setText(amount + '');
  }

  private updateStatus() {
    const state = this.getBettingPlayerState();
    if (state && state.active) {
      this.backgroundBorder.visible = true;
    } else {
      this.backgroundBorder.visible = false;
    }
    const handPlayer = this.getHandPlayer();
    if (handPlayer && handPlayer.status === HandPlayerStatus.OUT) {
      this.darken(this.backgroundImage);
      this.darken(this.cardOneImage);
      this.darken(this.cardTwoImage);
    } else {
      this.lighten(this.backgroundImage);
      this.lighten(this.cardOneImage);
      this.lighten(this.cardTwoImage);
    }
  }

  private darken(sprite: Phaser.Sprite | Phaser.Image) {
    this.setTint(sprite, 0x555555);
  }

  private lighten(sprite: Phaser.Sprite | Phaser.Image) {
    this.setTint(sprite, Phaser.Color.WHITE);
  }

  private setTint(sprite: Phaser.Sprite | Phaser.Image, tint: number) {
    if (sprite) {
      sprite.tint = tint;
    }
  }

  private clearCards() {
    if (this.cardOneImage) {
      this.cardOneImage.destroy();
    }
    if (this.cardTwoImage) {
      this.cardTwoImage.destroy();
    }
  }

  private displayCard(card: Card, x: number): Phaser.Image | Phaser.Sprite | undefined {
    if (!card) {
      return undefined;
    }

    const y = this.backgroundImage.height / 2;
    let cardImage;
    const handPlayerState = this.getHandPlayer();
    if (this.getHandState().cardsFaceUp && handPlayerState && handPlayerState.status !== HandPlayerStatus.OUT) {
      cardImage = this.game.add.sprite(x, y, Assets.Spritesheets.SpritesheetsCards167220.getName(), getFrame(card));
    } else {
      cardImage = this.game.add.image(x, y, Assets.Images.ImagesCardBack.getName());
    }
    cardImage.scale.setTo(.4);
    cardImage.anchor.setTo(0.5);
    this.backgroundImage.addChild(cardImage);
    return cardImage;
  }

  private createChatText() {
    const x = -this.backgroundImage.width / 2;
    const y = -this.backgroundImage.height;
    this.chatText = this.game.add.text(x, y, '', {
      font: '20px Arial',
      fill: 'white'
    });
    this.backgroundImage.addChild(this.chatText);
  }

  private createBetText() {
    const x = this.backgroundImage.width / 4;
    const y = -this.backgroundImage.height / 3;
    this.betText = this.game.add.text(x, y, '', {
      font: BET_FONT,
      align: 'right',
    });
    this.backgroundImage.addChild(this.betText);
  }

  private getPlayerState(): PlayerState | undefined {
    return this.tableStateSupplier.getTableState().getPlayer(this.playerId);
  }

  private getHandPlayer(): HandPlayerState | undefined {
    return this.getHandState().handPlayers.find(p => p.id === this.playerId);
  }

  private getBettingPlayerState(): BettingPlayerState | undefined {
    return this.getHandState().bettingState.bettingPlayerStates.find(p => p.id === this.playerId);
  }

  private getHandState(): HandState {
    return this.tableStateSupplier.getTableState().gameState.handState;
  }

  private centerX(): number {
    return 0;
  }

  private centerY(): number {
    return 0;
  }
}
