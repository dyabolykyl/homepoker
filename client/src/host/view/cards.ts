import { Card } from '../../common/models/Card';

export function getFrame(card: Card): number {
  return card.suit * 13 + card.value;
}
