import { BettingState, EMPTY_BETTING_STATE } from '../../common/game/BettingState';
import { EvaluatedHandPlayer, PokerEvaluator } from './PokerEvaluator';
import { HandPlayer, HandPlayerStatus } from '../../common/models/HandPlayer';

import { BettingCycle } from './BettingCycle';
import { Card } from '../../common/models/Card';
import { Deck } from '../../common/models/Deck';
import { GameOptions } from '../../common/models/GameOptions';
import { Hand } from 'pokersolver';
import { HandState } from '../../common/game/HandState';
import { Player } from '../../common/models/Player';
import { PlayerAction } from '../../common/game/PlayerAction';
import { Pot } from './Pot';
import { log } from '../../utils/log';

export enum Street {
  DEALING,
  PREFLOP,
  FLOP,
  TURN,
  RIVER,
  DONE
}

export const CARDS_IN_HAND = 2;

interface Winnings {
  amount: number;
  evaluatedHand?: Hand;
}

export class GameHand {
  
  cardsFaceUp: boolean;

  private readonly handId: number;
  private readonly players: HandPlayer[];
  private readonly deck: Deck;
  private readonly pot: Pot;
  private readonly board: Card[];
  private readonly gameOptions: GameOptions;
  private readonly pokerEvaluator: PokerEvaluator; 
  private readonly winnings: Map<string, Winnings> = new Map();

  private street: Street;
  private bettingCycle: BettingCycle;
  private statusMessage: string;

  constructor(gamePlayers: Player[], gameOptions: GameOptions, handId: number) {
    this.handId = handId;
    this.players = gamePlayers.map(p => new HandPlayer(p));
    log.debug(`Starting hand with players: ${gamePlayers}`);
    this.deck = new Deck();
    this.pot = new Pot(this.players);
    this.board = [];
    this.gameOptions = gameOptions;
    this.street = Street.DEALING;
    this.cardsFaceUp = false;
    this.pokerEvaluator = new PokerEvaluator();
  }

  public start() {
    this.evaluateState();
  }

  public getState(): HandState {
    return {
      handId: this.handId,
      board: this.board,
      handPlayers: this.players.map(p => p.getState()),
      cardsFaceUp: this.cardsFaceUp,
      potState: this.pot.getState(),
      bettingState: this.getBettingState(),
      statusMessage: this.statusMessage
    };
  }

  public getStreet(): Street {
    return this.street;
  }

  public processPlayerAction(action: PlayerAction): boolean {
    if (this.cardsFaceUp) {
      log.warn(`Cards have been shown, player actions no longer accepted`);
      return false;
    }

    if (!this.onBettingStreet() || !this.bettingCycle) {
      log.warn(`Not on a betting street`);
      return false;
    }

    if (!this.bettingCycle.processPlayerAction(action)) {
      return false;
    }

    const handPlayer = this.players.find(p => p.player.state.id === action.id);
    if (handPlayer) {
      this.statusMessage = `${handPlayer.player.state.name} ${action.actionType}s${action.amount ? ' ' + action.amount : ''}`;
    }
     
    if (this.bettingCycle.isComplete()) {
      this.evaluateState();
    }
    return true;
  }

  public finish() {
    log.info(`Determining winnings`);
    this.street = Street.DONE;

    this.determineWinnings();

    this.winnings.forEach((winnings, p) => { 
      log.debug(`${p} wins ${winnings}`);
      this.statusMessage += `. ${this.findPlayer(p).player.state.name} wins ${winnings.amount}`;
      if (winnings.evaluatedHand) {
        this.statusMessage += ` with ${winnings.evaluatedHand.descr}`;
      }
    });
  }

  public distributeWinnings(): void {
    log.debug(`Distributing winnings`);
    this.winnings.forEach((v, p) => this.findPlayer(p).player.adjustStack(v.amount));
  }

  public evaluateState() {
    const activePlayers = this.getActivePlayers();
    if (activePlayers.length === 1) {
      // show winner and pause, then reset
      log.info(`Only 1 active player left`);
      this.finish();
      return;
    }

    const bettingPlayers = activePlayers.filter(p => p.status !== HandPlayerStatus.ALL_IN);
    if (!this.cardsFaceUp && bettingPlayers.length <= 1) {
      this.returnUnusedChips();
      this.cardsFaceUp = true;
    }
    this.advanceStreet();
  }

  private determineWinnings(): void {
    this.returnUnusedChips();
    
    const activePlayers = this.getActivePlayers();
    
    let winners: EvaluatedHandPlayer[];
    if (activePlayers.length === 1) {
      winners = [{
        handPlayer: activePlayers[0]
      }];
    } else {
      winners = this.pokerEvaluator.solveGameHand(activePlayers, this.board);
    }

    winners.forEach(player => {
      this.pot.getSubPots().forEach(subPot => {
        if (subPot.isClosed()) {
          return;
        }

        if (subPot.getContribution(player.handPlayer.id) <= 0) {
          return;
        }

        subPot.close();
        if (!this.winnings.has(player.handPlayer.id)) {
          this.winnings.set(player.handPlayer.id, {
            amount: 0,
            evaluatedHand: player.evaluatedHand
          });
        }
        this.winnings.get(player.handPlayer.id).amount += subPot.getTotalAmount();
      });
    });
  }

  private returnUnusedChips() {
    log.debug(`Returning unused chips`);
    const unusedContribution = this.pot.getUnusedContribution();
    if (unusedContribution) {
      this.findPlayer(unusedContribution.id).player.adjustStack(unusedContribution.value);
    }
  }

  private onBettingStreet(): boolean {
    return [Street.PREFLOP, Street.FLOP, Street.TURN, Street.RIVER].some(s => s === this.street);
  }

  private advanceStreet() {
    const oldStreet = this.street;
    switch (this.street) {
      case Street.DEALING:
        this.street = this.beginHand();
        break;
      case Street.PREFLOP:
      case Street.FLOP:
      case Street.TURN:
        if (!this.cardsFaceUp) {
          this.resetBettingCycle();
        }
        this.street++;
        break;
      case Street.RIVER:
        this.cardsFaceUp = true;
        this.finish();
        break;
      default:
        log.warn(`Unhandled street: ${this.street}`);
    }
    this.updateBoard();
    log.debug(`Advanced street from ${Street[oldStreet]} to ${Street[this.street]}`);
  }

  private getActivePlayers(): HandPlayer[] {
    return this.players.filter(p => p.status !== HandPlayerStatus.OUT);
  }

  private findPlayer(id: string): HandPlayer {
    return this.players.find(p => p.id === id);
  }

  private beginHand(): Street {
    log.debug('Beginning hand');
    this.resetBettingCycle();
    this.takeBlinds();
    this.deal();
    return Street.PREFLOP;
  }

  private resetBettingCycle() {
    const playersInRound = this.players.filter(p => p.status !== HandPlayerStatus.OUT);
    this.bettingCycle = new BettingCycle(playersInRound, this.gameOptions.bigBlind, this.pot);
  }

  private takeBlinds(): void {
    log.debug('Taking blinds');
    this.bettingCycle.takeBlinds(this.gameOptions.smallBlind, this.gameOptions.bigBlind);
  }

  private deal() {
    log.debug('Dealing cards');
    for (let i = 0; i < CARDS_IN_HAND; i++) {
      this.players.forEach(p => p.playerHand.addCard(this.deck.draw()));
    }
  }

  private updateBoard() {
    let expectedCardCount = 0;
    switch (this.street) {
      case Street.FLOP:
        expectedCardCount = 3;
        break;
      case Street.TURN:
        expectedCardCount = 4;
        break;
      case Street.RIVER:
      // case Street.DONE:
        expectedCardCount = 5;
        break;
      default:
        break;        
    }

    while (this.board.length < expectedCardCount) {
      this.board.push(this.deck.draw());
    }
  }

  private getBettingState(): BettingState {
    if (!this.bettingCycle || this.bettingCycle.isComplete()) {
      return EMPTY_BETTING_STATE;
    }

    return this.bettingCycle.getBettingState();
  }
}