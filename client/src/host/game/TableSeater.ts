import { checkState } from '../../utils/preconditions';
import { log } from '../../utils/log';

export const MAX_PLAYERS = 8;

export class TableSeater {

  private readonly seats: number[] = [];

  constructor() {
    for (let i = 0; i < MAX_PLAYERS; i++) {
      this.seats.push(i);
    }
  }

  isFull() {
    return this.seats.length === 0;
  }

  getNextSeat = (): number => {
    checkState(!this.isFull(), 'Table is full');
    const seat = this.seats.pop();
    log.debug(`Using seat ${seat}`);
    return seat as number;
  }

  returnSeat(seat: number): void {
    if (seat > MAX_PLAYERS || seat < 1 || this.seats.indexOf(seat) >= 0) {
      log.warn(`Can't return seat. Invalid seat number: ${seat}`);
      return;
    }

    this.seats.push(seat);
  }

}