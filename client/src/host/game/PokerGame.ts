import { Executor, TimedExecutor } from '../../common/utils/Executor';
import { GameHand, Street } from './GameHand';
import { checkNotNull, checkState } from '../../utils/preconditions';

import { EMPTY_HAND } from '../../common/game/HandState';
import { GameOptions } from '../../common/models/GameOptions';
import { GameState } from '../../common/game/GameState';
import { MAX_PLAYERS } from './TableSeater';
import { Player } from '../../common/models/Player';
import { PlayerAction } from '../../common/game/PlayerAction';
import { log } from '../../utils/log';

export const AUTOPLAY_DELAY_MILLIS = 3000;

export class PokerGame {

  private readonly seats: (Player | undefined)[];
  private readonly onStateUpdated: () => void;
  private readonly executor: Executor;

  private isRunning: boolean;
  private currentHand: GameHand | undefined;
  private button: number;
  private gameOptions: GameOptions;
  private handId = 0;

  constructor(onStateUpdated: () => void, executor?: Executor) {
    this.onStateUpdated = onStateUpdated;
    this.executor = executor || new TimedExecutor();
    this.seats = [];
    this.button = -1;
    this.seats.fill(undefined, 0, MAX_PLAYERS);
  }

  public start(gameOptions: GameOptions) {
    checkState(!this.isRunning, 'game already started');
    this.gameOptions = checkNotNull(gameOptions, 'gameOptions');

    if (!this.canStart()) {
      log.warn(`Not enough players`);
      return;
    }

    this.isRunning = true;
    this.beginHand();
    this.onStateUpdated();
  }

  public addPlayer(player: Player): void {
    if (this.getPlayers().find(p => (p.state.id === player.state.id) || (p.state.seat === player.state.seat))) {
      log.warn(`Player ${player.state.id} already in game or seat ${player.state.seat} is taken`);
      return;
    }

    this.seats[player.state.seat] = player;
    if (this.button < 0) {
      this.button = player.state.seat;
      log.debug(`Set button to ${this.button}`);
    }
    log.debug(`Player joined game: ${player}`);
    this.onStateUpdated();
  }

  public processPlayerAction(action: PlayerAction): boolean {
    log.debug(`Processing: ${JSON.stringify(action)}`);
    if (!this.findPlayer(action.id)) {
      log.warn(`No player found`);
      return false;
    }

    if (!this.currentHand) {
      log.warn(`No hand in progress`);
      return false;
    }

    if (this.currentHand.processPlayerAction(action)) {
      this.evaluateHandState();
      this.onStateUpdated();
      return true;
    }

    return false;
  }

  public getState(): GameState {
    return {
      running: this.isRunning,
      handState: this.currentHand ? this.currentHand.getState() : EMPTY_HAND
    };
  }

  public canStart(): boolean {
    return (this.getActivePlayers().length >= 2);
  }

  private beginHand(): void {
    checkState(!this.currentHand, 'hand already in progress');

    const handPlayers = this.getActivePlayers();
    if (handPlayers.length < 2) {
      log.warn(`Can't start new hand. Not enough active players`);
      return;
    }
    this.advanceButton();
    // rotate handplayers so button is last
    for (let i = 0; i < handPlayers.length; i++) {
      const buttonIndex = handPlayers.length - 1;
      if (handPlayers[buttonIndex].state.seat === this.button) {
        break;
      }
      handPlayers.push(handPlayers.shift() as Player);
    }
    this.handId++;
    this.currentHand = new GameHand(handPlayers, this.gameOptions, this.handId);
    this.currentHand.start();
    this.onStateUpdated();
  }

  private evaluateHandState() {
    if (!this.currentHand) {
      log.warn(`No hand to evaluate`);
      return;
    }

    if (this.currentHand.getStreet() === Street.DONE || this.currentHand.cardsFaceUp) {
      this.autoPlayNextStreet();
    }
  }

  private autoPlayNextStreet = (prevStreet?: Street) => {
    let action: () => void;
    const currentStreet = this.currentHand.getStreet();
    checkState(prevStreet !== currentStreet, 'Street did not advance');
    switch (currentStreet) {
      // case Street.RIVER:
      case Street.PREFLOP:
      case Street.FLOP:
      case Street.TURN:
        action = () => {
          log.debug(`Autoplaying next street`);
          this.currentHand.evaluateState();
          this.autoPlayNextStreet(currentStreet);
        };
        break;
      case Street.RIVER:
        this.currentHand.evaluateState();
      // tslint:disable-next-line
      case Street.DONE:
        log.debug(`Finishing hand`);
        action = () => {
          this.currentHand.evaluateState();
          this.currentHand.distributeWinnings();
          this.currentHand = undefined;
          this.beginHand();
        };
        break;
      default:
        break;
    }

    const fullAction = () => {
      action();
      this.onStateUpdated();
      log.debug(`Auto advanced street from ${Street[currentStreet]} to ${Street[currentStreet + 1]}`);
    };

    this.executor.submit(fullAction, AUTOPLAY_DELAY_MILLIS);
  }

  private getPlayers(): Player[] {
    return this.seats.filter(p => p) as Player[];
  }

  private getActivePlayers(): Player[] {
    return this.seats.filter(p => p && p.isActive()) as Player[];
  }

  private findPlayer(playerId: string): Player | undefined {
    return this.getPlayers().find(p => (p.state.id === playerId));
  }

  private advanceButton() {
    let newButton = this.button;
    do {
      newButton = (newButton + 1) % MAX_PLAYERS;
      const player = this.seats[newButton];
      if (player && player.isActive()) {
        break;
      }
    } while (newButton !== this.button);

    log.debug(`Advanced button from ${this.button} to ${newButton}`);
    this.button = newButton;
  }

}