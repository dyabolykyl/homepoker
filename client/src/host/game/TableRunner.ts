import { Event, EventBus, EventType } from '../../common/utils/Emitter';
import { INVALID_REQUEST, INVALID_TABLE, Response, SUCCESS, ViewTableResponse, hasFailed } from '../../common/socket/Response';
import { PlayerActionRequest, Request, SocketEvent, UpdatePlayerStateRequest, UpdateViewRequest, ViewTableRequest, makeHostTableRequest } from '../../common/socket/Request';

import { FullPlayerState } from '../../common/game/TableState';
import { GameOptions } from '../../common/models/GameOptions';
import { GameView } from '../view/GameView';
import { HandState } from '../../common/game/HandState';
import { Lifecycle } from '../../common/utils/Lifecycle';
import { Player } from '../../common/models/Player';
import { PokerGame } from './PokerGame';
import { SessionManager } from '../../socket/SessionManager';
import { TableStateSupplier } from './TableStateSupplier';
import { log } from '../../utils/log';

const useStaticTableId = false;

export interface TableRunner {

  setGameView(gameView: GameView): void;

  onPlayerJoined(player: Player): void;

  onChat(playerId: string, message: string): void;

  onStartGame(): boolean;

  viewTable(tableId: string): void;
}

export class TableIdSetEvent implements Event {
  eventType = EventType.HOST_TABLE_ID_SET;
  readonly tableId: string;

  constructor(tableId: string) {
    this.tableId = tableId;
  }
}

export class TableRunnerImpl implements TableRunner, Lifecycle  {

  private readonly sessionManager: SessionManager;
  private readonly tableStateSupplier: TableStateSupplier;
  private readonly eventBus: EventBus;
  
  private game: PokerGame;

  private gameView: GameView;

  constructor(sessionManager: SessionManager,
              tableStateSupplier: TableStateSupplier,
              eventBus: EventBus) {
    this.sessionManager = sessionManager;
    this.tableStateSupplier = tableStateSupplier;
    this.eventBus = eventBus;
    this.game = new PokerGame(this.onStateUpdated);
  }

  setGameView(gameView: GameView) {
    this.gameView = gameView;
  }

  start() {
    this.sessionManager.onConnect(this.hostTable);
    this.sessionManager.listen(SocketEvent.UPDATE_VIEW, this.onUpdateView);
    this.sessionManager.listen(SocketEvent.VIEW_TABLE, this.onViewerRegistered);
    this.sessionManager.listen(SocketEvent.PLAYER_ACTION, this.onPlayerAction);
  }

  stop() {

  }

  onPlayerJoined = (player: Player) => {
    this.updateView();
    this.game.addPlayer(player);
    this.sendViewUpdate();
  }

  onChat = (playerId: string, message: string) => {
    const playerState = this.tableStateSupplier.getTableState().getPlayer(playerId);
    if (!playerState) {
      return;
    }

    playerState.chatMessage = message;
    this.gameView.displayChatMessage(playerId);
    this.sendViewUpdate();
  }

  onStartGame = (): boolean => {
    if (!this.game.canStart()) {
      return false;
    }

    // TODO: get game options from somewhere
    const gameOptions: GameOptions = {
      smallBlind: 1,
      bigBlind: 2
    };
    this.game.start(gameOptions);
    this.onStateUpdated();
    this.eventBus.post({ eventType: EventType.GAME_STARTED });
    return true;
  }

  async viewTable(tableId: string) {
    const res: ViewTableResponse = await this.sessionManager.send(new ViewTableRequest(tableId)) as ViewTableResponse;
    if (hasFailed(res)) {
      log.warn(`Failed to view game: ${res.result}`);
    } else {
      if (!res.tableState) {
        log.warn(`Missing game state`);
        return;
      }
      this.gameView.enableGameViewing(tableId);
      this.tableStateSupplier.setViewing(true);
      this.tableStateSupplier.setTableState(res.tableState);
      this.gameView.updateView();
    }
  }

  private hostTable = async (): Promise<void> => {
    const tableId = this.createTableId();
    log.info(`Attempting to host game: ${tableId}`);
    
    try {
      const res = await this.sessionManager.send(makeHostTableRequest(tableId));
      if (!hasFailed(res)) {
        log.debug(`Host game success`);
        this.setTableId(tableId);
      }
    } catch (err) {
      log.warn(`Error while attempting to host game: ${err}\n${err.stack}`);
    }

  }

  private onStateUpdated = () => {
    this.tableStateSupplier.getTableState().gameState = this.game.getState();
    this.updateView();
    this.sendViewUpdate();
    this.sendPlayerUpdates();
  }

  private sendPlayerUpdates() {
    const tableState = this.tableStateSupplier.getTableState();
    tableState.getFullPlayerStates().forEach(fullPlayerState => {
      const req = new UpdatePlayerStateRequest(tableState.tableId, fullPlayerState);
      this.sessionManager.send(req);
    });
  }

  private sendViewUpdate() {
    const tableState = this.tableStateSupplier.getTableState();
    const req = new UpdateViewRequest(tableState.tableId, tableState);
    this.sessionManager.send(req);
  }

  private onUpdateView = (req: UpdateViewRequest): Response => {
    log.debug(`Received update view request for ${req.tableId}`);
    if (!this.tableStateSupplier.isViewing()) {
      log.warn(`Not viewing a table`);
      return INVALID_REQUEST;
    }

    if (!req.tableId || !req.tableState) {
      log.warn(`Request is missing table id or game state`);
      return INVALID_REQUEST;
    }

    this.tableStateSupplier.setTableState(req.tableState);
    this.gameView.updateView();
    return SUCCESS;
  }

  private onViewerRegistered = (req: Request): ViewTableResponse => {
    log.debug(`New viewer registered`);
    return new ViewTableResponse(this.tableStateSupplier.getTableState());
  }

  private onPlayerAction = (req: PlayerActionRequest): Response => {
    log.debug(`Processing player action request: ${JSON.stringify(req)}`);
    const tableState = this.tableStateSupplier.getTableState();
    if (tableState.tableId !== req.tableId) {
      log.warn(`Invalid table id`);
      return INVALID_TABLE;
    }

    if (this.tableStateSupplier.isViewing()) {
      log.warn(`Table is in view mode`);
      return INVALID_TABLE;
    }

    if (!tableState.getPlayer(req.uuid)) {
      log.warn(`${req.uuid} not in game`);
      return INVALID_REQUEST;
    }

    if (req.uuid !== req.action.id) {
      log.warn(`Recieved action from ${req.uuid} for a different player: ${req.action.id}`);
      return INVALID_REQUEST;
    }

    const accepted = this.game.processPlayerAction(req.action);
    log.debug(`Action ${accepted ? 'accepted' : 'rejected'}`);
    return accepted ? SUCCESS : INVALID_REQUEST;
  }

  private updateView() { 
    if (this.gameView) {
      this.gameView.updateView();
    }
  }

  private createTableId(): string {
    return useStaticTableId ? '1' : Math.floor(Math.random() * 99999 + 100000) + '';
  }

  private setTableId = (tableId: string) => {
    log.info(`New GameID: ${tableId}`);
    this.tableStateSupplier.getTableState().tableId = tableId;
    this.eventBus.post(new TableIdSetEvent(tableId));
  }
}
