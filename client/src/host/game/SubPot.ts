import { checkState } from '../../utils/preconditions';
import { log } from '../../utils/log';

export interface Contributor {
  id: string;
  value: number;
}

export class SubPot {

  private readonly contributions: Map<string, number> = new Map();
  private limit: number = 0;
  private locked: boolean;
  private closed: boolean;

  constructor() {
  }

  public addBet(player: string, amount: number): number {
    // log.debug(`Adding ${amount} from ${player} to subpot`);
    checkState(!this.contributions.has(player), `${player} already contributed to subpot`);
    const amountToAdd = this.locked ? Math.min(this.limit, amount) : amount;
    this.contributions.set(player, amountToAdd);
    return amountToAdd;
  }

  public lock(player: string): void {
    checkState(this.contributions.has(player), `${player} hasn't contributed to subpot`);
    this.limit = this.contributions.get(player);
    this.locked = true;
  }

  public close() {
    this.closed = true;
  }

  public isLocked(): boolean {
    return this.locked;
  }

  public isClosed(): boolean {
    return this.closed;
  }

  public getContribution(player: string): number {
    return this.contributions.get(player) || 0;
  }

  public getContributions(): Map<string, number> {
    return this.contributions;
  }

  public getTotalAmount(): number {
    return Array.from(this.contributions.values()).reduce((sum, value) => sum + value, 0);
  }

  public getUnusedContribution(): Contributor | undefined {
    if (this.contributions.size === 1) {
      log.debug(`Only one contributor to sub pot`);
      this.close();
      const contributor = this.contributions.entries().next().value;
      return {
        id: contributor[0],
        value: contributor[1]
      };
    } else {
      const contributorsByBet: Contributor[] = Array.from(this.contributions.entries())
        .slice(0)
        .map(c => ({ id: c[0], value: c[1] }))
        .sort((a, b) => b.value - a.value);
      const largestContributor = contributorsByBet[0];
      const secondLargestContributor = contributorsByBet[1];
      const unused = largestContributor.value - secondLargestContributor.value;
      if (unused > 0) {
        log.debug(`${largestContributor.id} has bet more than second largest contributor (${largestContributor.value} > ${secondLargestContributor.value})`);
        this.contributions.set(largestContributor.id, secondLargestContributor.value);
        return {
          id: largestContributor.id,
          value: unused 
        };
      }
    }
  }

}