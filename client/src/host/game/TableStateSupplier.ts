import { TableState } from '../../common/game/TableState';

export class TableStateSupplier {

  private viewing: boolean = false;
  private tableState: TableState = new TableState();

  getTableState(): TableState {
    return this.tableState;
  }

  setTableState(tableState: TableState) {
    this.tableState = tableState;
  }

  isViewing() {
    return this.viewing;
  }

  setViewing(viewing: boolean) {
    this.viewing = viewing;
  }
}
