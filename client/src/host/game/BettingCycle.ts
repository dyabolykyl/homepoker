import { ActionType, PlayerAction } from '../../common/game/PlayerAction';
import { HandPlayer, HandPlayerStatus } from '../../common/models/HandPlayer';

import { BettingPlayer } from '../../common/models/BettingPlayer';
import { BettingState } from '../../common/game/BettingState';
import { Pot } from './Pot';
import { log } from '../../utils/log';

export class BettingCycle {

  readonly bettingPlayers: BettingPlayer[];
  private currentIndex: number;
  private currentBet: number;
  private minimumRaise: number;
  private completed: boolean;

  private pot: Pot;

  constructor(players: HandPlayer[], bigBlind: number, pot: Pot) {
    this.bettingPlayers = players.map(p => new BettingPlayer(p));
    this.currentIndex = 0;
    this.currentBet = 0;
    this.minimumRaise = bigBlind;
    this.completed = false;
    this.pot = pot;

    log.info(`New betting cycle. Waiting on ${this.currentPlayer()}`);
  }

  public getBettingState(): BettingState {
    return {
      bettingPlayerStates: this.bettingPlayers.map(p => ({
        id: p.handPlayer.id,
        active: p.handPlayer.id === this.currentPlayer().handPlayer.id,
        activeBet: p.totalBet,
        canCheck: p.totalBet === this.currentBet
      }))
    };
  }

  public processPlayerAction(action: PlayerAction): boolean {
    if (this.completed) {
      log.warn(`Betting cylce is complete`);
      return false;
    }

    if (action.id !== this.currentPlayer().handPlayer.player.state.id) {
      log.warn(`Out of turn action from ${action.id}`);
      return false;
    }

    let accepted = false;
    switch (action.actionType) {
      case ActionType.FOLD:
        accepted = this.handleFold();
        break;
      case ActionType.CHECK:
        accepted = this.handleCheck();
        break;
      case ActionType.CALL:
        accepted = this.handleCall();
        break;
      case ActionType.BET:
        accepted = this.handleBet(action.amount);
        break;
      default:
        log.warn(`Unknown player action: ${action.actionType}`);
    }
    
    if (accepted) {
      this.currentPlayer().considered = true;
      this.advanceIndex();
    }

    return accepted;
  }

  public takeBlinds(smallBlind: number, bigBlind: number) {
    if (this.bettingPlayers.length === 2) {
      this.advanceIndex();
    }
    
    this.takeBetFromCurrentPlayer(smallBlind);
    this.advanceIndex();
    this.takeBetFromCurrentPlayer(bigBlind);
    this.advanceIndex();
  }

  public isComplete(): boolean {
    return this.completed;
  }

  private handleFold(): boolean {
    this.currentPlayer().handPlayer.sitOut();
    return true;
  }

  private handleCheck(): boolean {
    if (this.currentBet !== this.currentPlayer().totalBet) {
      log.warn(`Bet is not equal to table bet`);
      return false;
    }

    return true;
  }

  private handleCall(): boolean {
    const amountToCall = this.currentBet - this.currentPlayer().totalBet;
    if (amountToCall <= 0) {
      log.warn(`Amount to call is invalid`);
      return false;
    }

    this.takeBetFromCurrentPlayer(amountToCall);
    return true;
  }

  private handleBet(bet: number | undefined): boolean {
    if (!bet) {
      log.warn(`No bet amount specified`);
      return false;
    }

    if (bet > this.currentPlayer().handPlayer.player.state.stack) {
      log.warn(`Player has insufficient stack`);
      return false;
    }

    const minimumBet = this.currentBet - this.currentPlayer().totalBet + this.minimumRaise;
    if (bet < minimumBet && this.currentPlayer().handPlayer.player.state.stack !== bet) {
      log.warn(`Minimum bet of ${minimumBet} required`);
      return false;
    }

    this.takeBetFromCurrentPlayer(bet);
    return true;
  }

  private takeBetFromCurrentPlayer(bet: number) {
    const amount = this.bettingPlayers[this.currentIndex].takeBet(bet);
    if (this.currentPlayer().totalBet > this.currentBet) {
      this.minimumRaise = 2 * (this.currentPlayer().totalBet - this.currentBet);
      this.currentBet = this.currentPlayer().totalBet;
    }
    this.pot.calculateSubPots();
    log.debug(`Took bet from on ${this.currentPlayer()}`);
  }

  private advanceIndex(): void {
    let newIndex = -1;
    let activePlayers = this.bettingPlayers.filter(p => p.handPlayer.status !== HandPlayerStatus.OUT);
    if (activePlayers.length < 2) {
      this.complete();
      return;
    }

    for (let i = 1; i <= this.bettingPlayers.length; i++) {
      const index = (this.currentIndex + i) % this.bettingPlayers.length;
      const player = this.bettingPlayers[index];

      if (player.handPlayer.status !== HandPlayerStatus.IN) {
        continue;
      }

      if (!player.considered || player.totalBet < this.currentBet) {
        newIndex = index;
        break;
      } 
    }
    
    if (newIndex < 0) {
      this.complete();
    } else {
      this.currentIndex = newIndex;
      log.info(`Waiting on ${this.currentPlayer()}`);
    }
  }

  private complete() {
    log.debug('Betting cycle completed');
    this.completed = true; 
  }

  private currentPlayer(): BettingPlayer {
    return this.bettingPlayers[this.currentIndex];
  }
}
