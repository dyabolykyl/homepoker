import * as _ from 'lodash';

import { Contributor, SubPot } from './SubPot';
import { HandPlayer, HandPlayerState, HandPlayerStatus } from '../../common/models/HandPlayer';

import { PotState } from '../../common/game/PotState';
import { log } from '../../utils/log';

export class Pot {

  private totalAmount: number = 0;
  private subPots: SubPot[] = [];
  private readonly players: HandPlayer[];

  constructor(players: HandPlayer[]) {
    this.players = players;
  }

  public calculateSubPots() {
    this.subPots = [];
    const activePlayers: Map<boolean, HandPlayer[]> = new Map();
    [true, false].forEach(s => activePlayers.set(s, []));
    const playersByInitialStack = this.players
      .filter(p => p.totalBet > 0)
      .slice(0)
      .sort((a, b) => a.initialStack - b.initialStack)
      .forEach(player => activePlayers.get(player.status !== HandPlayerStatus.OUT).push(player));

    // fill sub pots with active players, smallest stack first
    // log.debug(`Filling subpots with ${activePlayers.get(true).length} active players`);
    activePlayers.get(true).forEach(this.fillSubPots);

    // add folded players bets to pot
    // log.debug(`Filling subpots with ${activePlayers.get(false).length} non-active players`);
    activePlayers.get(false).forEach(this.fillSubPots);
  }

  getState(): PotState {
    return {
      subPots: this.subPots.map(subPot => subPot.getTotalAmount())
    };
  }

  getSubPots(): SubPot[] {
    return this.subPots;
  }

  getTotalAmount(): number {
    return this.subPots.map(subPot => subPot.getTotalAmount()).reduce((sum, value) => sum + value, 0);
  }

  getUnusedContribution(): Contributor {
    const lastSubPot = this.subPots[this.subPots.length - 1];
    const unusedContribution = lastSubPot.getUnusedContribution();
    if (unusedContribution) {
      if (lastSubPot.isClosed()) {
        this.subPots.pop();
      }
    }
    return unusedContribution;
  }
  
  private fillSubPots = (player: HandPlayer) => {
    let potIndex = 0;
    let totalBet = player.totalBet;
    let subPot: SubPot;
    while (totalBet > 0) {
      subPot = this.getNextSubPot(potIndex++);
      totalBet -= subPot.addBet(player.id, totalBet);
    }

    if (player.initialStack === player.totalBet) {
      subPot.lock(player.id);
    }
  }

  private getNextSubPot = (index: number): SubPot => {
    if (index === this.subPots.length) {
      this.subPots.push(new SubPot());
    }
    return this.subPots[index];
  }
}
