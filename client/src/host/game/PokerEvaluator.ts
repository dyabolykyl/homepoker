import * as _ from 'lodash';

import { Card } from '../../common/models/Card';
import { Hand } from 'pokersolver';
import { HandPlayer } from '../../common/models/HandPlayer';
import { Lifecycle } from '../../common/utils/Lifecycle';
import { PlayerHand } from '../../common/models/PlayerHand';
import { PokerHand } from 'pokersolver';
import { Suit } from '../../common/models/Suit';
import { Value } from '../../common/models/Value';
import { log } from '../../utils/log';

export interface EvaluatedHandPlayer {
  handPlayer: HandPlayer;
  evaluatedHand?: Hand;
}

export class PokerEvaluator {

  public solveGameHand(players: HandPlayer[], board: Card[]): EvaluatedHandPlayer[] {
    const handsByPlayer: EvaluatedHandPlayer[] = players.map(handPlayer => ({
      handPlayer,
      evaluatedHand: this.convertPlayerHand(handPlayer.playerHand, board)
    }));

    const winners = Hand.winners(handsByPlayer.map(h => h.evaluatedHand));
    const winnersByPlayer = winners.map(hand =>
      handsByPlayer.find(player => 
        _.isMatch(player.evaluatedHand.cardPool, hand.cardPool)));
    return winnersByPlayer;    
  }

  private convertPlayerHand(hand: PlayerHand, board: Card[]): Hand {
    const pokerHand = board.map(this.convertCard);
    hand.cards.forEach(card => pokerHand.push(this.convertCard(card)));
    return Hand.solve(pokerHand);
  }

  private convertCard = (card: Card): string => {
    let value;
    if (card.value > Value.ACE && card.value < Value.TEN) {
      value = card.value + 1 + '';
    } else {
      value = (Value[card.value] as string).substr(0, 1).toUpperCase();
    }
    
    let suit = (Suit[card.suit] as string).substr(0, 1).toLowerCase();
    // log.debug(`Converted ${card} to ${value + suit}`);
    return value + suit;
  }
}
