import * as Assets from '../../assets';

import { EventBus, EventType } from '../../common/utils/Emitter';
import { TableIdSetEvent, TableRunner } from '../game/TableRunner';

import { BaseState } from '../../states/base';
import { BoardView } from '../view/BoardView';
import { Context } from '../../utils/context';
import { GameView } from '../view/GameView';
import { PhaserInput } from '../../phaser-input/Plugin';
import { PlayerView } from '../view/PlaverView';
import { TableStateSupplier } from '../game/TableStateSupplier';
import { checkNotNull } from '../../utils/preconditions';
import { log } from '../../utils/log';

interface PlayerPosition {
    xMultiplier: number;
    yMultiplier: number;
}

const PLAYER_POSITIONS: PlayerPosition[] = [
    {
        xMultiplier: -1 / 6,
        yMultiplier: - 1 / 3
    },
    {
        xMultiplier: 0,
        yMultiplier: - 1 / 3
    },
    {
        xMultiplier: 1 / 6,
        yMultiplier: - 1 / 3
    },
    {
        xMultiplier: 2 / 7,
        yMultiplier: 0
    },
    {
        xMultiplier: 1 / 6,
        yMultiplier: 2 / 7
    },
    {
        xMultiplier: 0,
        yMultiplier: 2 / 7
    },
    {
        xMultiplier: - 1 / 6,
        yMultiplier: 2 / 7
    },
    {
        xMultiplier: -2 / 7,
        yMultiplier: 0
    },
];

export default class HostMain extends BaseState implements GameView {

    private tableStateSupplier: TableStateSupplier;
    private tableRunner: TableRunner;
    private eventBus: EventBus;

    private tableIdText: Phaser.Text;
    private viewedGameText: Phaser.Text;
    private landingGroup: Phaser.Group;
    private table: Phaser.Image;
    private playerViews: Map<string, PlayerView> = new Map();
    private startGameButton: Phaser.Button;
    private boardView: BoardView;
    private statusText: Phaser.Text;

    init(context: Context) {
        super.init(context);
        log.debug(`Initializing main scene`);
        this.tableStateSupplier = checkNotNull(context.getComponent('TableStateSupplier'), 'tableStateSupplier');
        this.tableRunner = checkNotNull(context.getComponent('TableRunner'), 'tableRunner');
        this.eventBus = checkNotNull(context.getComponent('EventBus'), 'eventBus');
        this.tableRunner.setGameView(this);
        this.eventBus.subscribe(EventType.HOST_TABLE_ID_SET, this.onTableIdSet);

        this.boardView = new BoardView(this.game, this.tableStateSupplier);
        this.createStatusText();
    }

    public create(): void {
        this.game.add.plugin(new PhaserInput.Plugin(this.game, this.game.plugins));
        this.game.camera.flash(0x000000, 500);
        this.game.stage.setBackgroundColor('#252628');
        document.getElementsByTagName('body')[0].style.background = '#252628';

        this.table = this.game.add.image(
            this.game.world.centerX,
            this.game.world.centerY,
            Assets.Images.ImagesTable.getName());
        this.table.anchor.setTo(0.5);
        this.table.scale = new Phaser.Point(1.5, 1.5);

        this.createTableIdText();
        this.createLandingGroup();

        this.input.onDown.add(this.onClick);        
        this.updateView();
    }

    public resize(width: number, height: number) {
        console.log(`Resizing: ${width}, ${height}`);
    }

    public addPlayer = (playerId: string): void => {
        const playerState = this.tableStateSupplier.getTableState().getPlayer(playerId);
        if (!playerState) {
            return;
        }
        log.debug(`Creating player view for ${playerId}`);

        const position = PLAYER_POSITIONS[playerState.seat];
        const playerHolder = this.game.add.group();
        this.table.addChild(playerHolder);

        const playerView: PlayerView = new PlayerView(
            playerId, playerHolder, 
            position.xMultiplier * this.table.width, position.yMultiplier * this.table.height,
            this.game, this.tableStateSupplier);
        this.playerViews.set(playerState.id, playerView);

        this.landingGroup.destroy();
        if (!this.startGameButton && !this.tableStateSupplier.getTableState().gameState.running) {
            this.createStartGameButton();
        }
        this.updateView();
    }

    public displayChatMessage(playerId: string): void {
        const playerView = this.playerViews.get(playerId);
        if (!playerView) {
            log.warn(`Missing player view for ${playerId}`);
            return;
        }

        playerView.updateChatMessage();
    }

    public enableGameViewing(tableId: string): void {
        if (this.tableIdText) {
            this.tableIdText.destroy();
        }
        this.viewedGameText = this.add.text(this.game.width - 500, 50);
        this.viewedGameText.anchor.setTo(0.5);
        this.viewedGameText.setStyle({ font: 'Arial', fontSize: 40, fill: 'white' });
        this.viewedGameText.setText(`Viewing game: ${tableId}`);
        this.landingGroup.destroy();
    }

    public updateView = () => {
        const tableState = this.tableStateSupplier.getTableState();
        log.debug(`Updating views`);
        log.debug(tableState.gameState);
        const deadPlayers = new Set(this.playerViews.keys());

        tableState.playerStates.forEach(playerState => {
            if (deadPlayers.has(playerState.id)) {
                const playerView = this.playerViews.get(playerState.id) as PlayerView;
                playerView.update();
                deadPlayers.delete(playerState.id);
            } else {
                this.addPlayer(playerState.id);
            }
        });

        deadPlayers.forEach(playerId => {
            (this.playerViews.get(playerId) as PlayerView).destroy();
        });
        
        this.boardView.update();
        this.updateStatusText();
    }

    private createTableIdText(): void {
        if (this.viewedGameText) {
            this.viewedGameText.destroy();
        }
        this.tableIdText = this.add.text(this.game.width - 400, 50);
        this.tableIdText.anchor.setTo(0.5);
        this.tableIdText.setStyle({ font: 'Arial', fontSize: 40, fill: 'white' });
        this.updateTableId();
    }

    private onTableIdSet = (event: TableIdSetEvent) => {
        this.updateTableId();
    }

    private updateTableId = () => {
        if (!this.tableIdText) {
            return;
        }

        let id = this.tableRunner ? this.tableStateSupplier.getTableState().tableId : '';
        log.debug(`Updating tableIdView: ${id}`);
        this.tableIdText.setText(`Table ID: ${id}`);
    }

    private createLandingGroup() {
        this.landingGroup = this.game.add.group();
        const waitingText = this.game.add.text(
            this.centerX, this.centerY - 50,
            'Waiting for players to join', { font: '50px Arial', fill: 'white' });
        waitingText.anchor.setTo(0.5);
        this.landingGroup.addChild(waitingText);

        const inputWidth = 300;
        const inputHeight = 50;
        const inputX = this.centerX - 300;
        const inputY = this.centerY + 20;
        const inputOptions = {
            width: inputWidth,
            height: inputHeight,
            font: '50px Arial',
            placeHolder: 'Table ID...',
            padding: 10,
            zoom: false
        };
        const viewGameInput = (this.game as PhaserInput.InputFieldGame).add.inputField(inputX, inputY, inputOptions);
        this.landingGroup.addChild(viewGameInput);

        const viewGameButton = this.game.add.button(
            this.centerX + 150, this.centerY + 57,
            Assets.Atlases.AtlasesButton.getName(),
            () => this.viewGame(viewGameInput.value), this, 1, 2, 0);
        viewGameButton.anchor.setTo(0.5);
        const viewGameText = this.game.add.text(0, 0, 'View Table');
        viewGameText.anchor.setTo(0.5);
        viewGameButton.addChild(viewGameText);
        this.landingGroup.addChild(viewGameButton);
    }

    private createStartGameButton() {
        this.startGameButton = this.game.add.button(
            this.centerX, this.centerY,
            Assets.Atlases.AtlasesButton.getName(),
            () => this.startGame(), this, 1, 2, 0);
        this.startGameButton.anchor.setTo(.5);
        const startGameText = this.game.add.text(0, 0, 'Start Game');
        startGameText.anchor.setTo(0.5);
        this.startGameButton.addChild(startGameText);
    }

    private createStatusText() {
        this.statusText = this.game.add.text(this.centerX, this.world.height / 9, '', {
            font: 'Arial', 
            fontSize: 40,
             fill: 'white'
        });
        this.statusText.anchor.set(0.5);
    }

    private updateStatusText() {
        const handState = this.tableStateSupplier.getTableState().gameState.handState;
        this.statusText.text = handState.statusMessage || '';
    }

    private startGame = (): void => {
        if (this.tableRunner.onStartGame()) {
            this.startGameButton.destroy();
            this.updateView();
        }
    }

    private viewGame = (tableId: string): void => {
        if (!tableId || tableId === '') {
            log.warn('Missing table id');
            return;
        }
        log.debug(`Attempting to view table ${tableId}`);
        this.tableRunner.viewTable(tableId);
    }

    private onClick(pointer: Phaser.Pointer): void {
        log.debug(`Clicked (${pointer.x}, ${pointer.y})`);
    }
}
