import * as Assets from '../../assets';

import { BaseState } from '../../states/base';
import { Context } from '../../utils/context';
import { InputField } from '../../phaser-input/InputField';
import { MainMenuView } from '../view/MainMenuView';
import { PhaserInput } from '../../phaser-input/Plugin';
import { TableManager } from '../game/TableManager';
import { log } from '../../utils/log';

const INPUT_WIDTH = 500;
const INPUT_HEIGHT = 100;

export default class PlayerMain extends BaseState implements MainMenuView {

  private tableManager: TableManager;

  private joinGameButton: Phaser.Button;
  private tableIdInput: InputField;
  private nameInput: InputField;

  public init(context: Context) {
    super.init(context);
    this.tableManager = context.getComponent('TableManager');

    this.tableManager.mainMenuView = this;
  }

  public create(): void {
    this.game.add.plugin(new PhaserInput.Plugin(this.game, this.game.plugins));
    this.game.stage.setBackgroundColor('#000C26');
    this.input.onDown.add(this.onClick); 
    document.getElementsByTagName('body')[0].style.background = '#000C26';

    this.createJoinGameButton();
    this.createTableIdInput();
    this.createNameInput();

    this.checkIfFake();       
  }

  public showGameScreen() {
    this.game.state.start('hand', undefined, undefined, this.context);
  }

  private onJoinGameClick = () => {
    const tableId = this.tableIdInput.value;
    const name = this.nameInput.value;
    log.debug(`join table clicked. tableIdInput: ${tableId} name: ${name}`);

    if (!tableId || !name) {
      log.warn(`Missing table ID or name`);
      return;
    }

    this.tableManager.joinTable(tableId, name);
  }

  // private disableButton() {
  //   this.joinGameButton.inputEnabled = false;
  //   this.joinGameButton.frame = 0;
  // }

  private createJoinGameButton() {
    this.joinGameButton = this.game.add.button(
      this.centerX, 
      this.centerY, 
      Assets.Atlases.AtlasesButton.getName(), 
      this.onJoinGameClick, this, 1, 2, 0);
    this.joinGameButton.anchor.setTo(0.5);
    this.joinGameButton.scale.setTo(2);
    const joinGameText = this.game.add.text(0, 0, 'Join Table');
    joinGameText.anchor.setTo(0.5);
    this.joinGameButton.addChild(joinGameText);
  }

  private createTableIdInput() {
    this.tableIdInput = this.createInputField('Table ID...', 25, this.centerY - INPUT_HEIGHT * 3);
  }

  private createNameInput() {
    this.nameInput = this.createInputField('Name...', this.centerX * 2 - INPUT_WIDTH - 25, this.centerY - INPUT_HEIGHT * 3);
  }

  private createInputField(placeHolder: string, x: number, y: number): InputField {
    const inputOptions = {
      width: INPUT_WIDTH,
      height: INPUT_HEIGHT,
      font: '80px Arial',
      placeHolder,
      padding: 10,
      zoom: false
    };
    return (this.game as PhaserInput.InputFieldGame).add.inputField(x, y, inputOptions); 
  }

  private checkIfFake() {
    if (FAKE) {
      this.tableManager.joinTable('123', 'p1');
    }
  }

  private onClick(pointer: Phaser.Pointer): void {
      // log.debug(`Clicked (${pointer.x}, ${pointer.y})`);
  }
}
