import * as Assets from '../../assets';

import { BettingPlayerState, BettingState } from '../../common/game/BettingState';
import { EventBus, EventType } from '../../common/utils/Emitter';

import { BaseState } from '../../states/base';
import { Card } from '../../common/models/Card';
import { FullPlayerState } from '../../common/game/TableState';
import { HandPlayerState } from '../../common/models/HandPlayer';
import { HandState } from '../../common/game/HandState';
import { InputHandler } from '../game/InputHandler';
import { PlayerState } from '../../common/game/PlayerState';
import { TableManager } from '../game/TableManager';
import { checkNotNull } from '../../utils/preconditions';
import { getFrame } from '../../host/view/cards';
import { log } from '../../utils/log';

export interface PlayerStateSupplier {
  getState: () => FullPlayerState | undefined;
}

type PhaserImage = Phaser.Image | Phaser.Sprite;

const DEFAULT_FONT = '100px Arial';
const BUTTON_FONT = '40px Arial';
const BET_AMOUNT_FONT = '80px Arial';

export class MobileHandState extends BaseState {

  private inputHandler: InputHandler;
  private playerStateSupplier: PlayerStateSupplier;

  private stackText: Phaser.Text;
  private stackAmountText: Phaser.Text;
  private positionText: Phaser.Text;

  private foldButton: Phaser.Button;
  private checkCallButton: Phaser.Button;
  private betRaiseButton: Phaser.Button;

  private betSliderBack: Phaser.Image;
  private betSliderKnob: Phaser.Image;
  private betAmountText: Phaser.Text;
  private betSliderKnobMinX: number;
  private betSliderKnobMaxX: number;

  private inputs: (Phaser.Button | Phaser.Image)[] = [];
  private active: boolean;

  private cardZone: Phaser.Graphics;
  private cardOneImage: PhaserImage;
  private cardTwoImage: PhaserImage;
  private touchingCards: boolean;

  public create(): void {
    this.game.stage.setBackgroundColor('#000C26');

    this.inputHandler = checkNotNull(this.context.getComponent('InputHandler'), 'inputHandler');
    this.playerStateSupplier = checkNotNull(this.context.getComponent('TableManager'), 'playerStateSupplier');
    const eventBus: EventBus = checkNotNull(this.context.getComponent('EventBus'), 'eventBus');
    eventBus.subscribe(EventType.PLAYER_STATE_UPDATED, this.updateView);
    this.input.onDown.add(this.onClick);      

    this.createStackText();
    this.createFoldButton();
    this.createCheckCallButton();
    this.createBetRaiseButton();
    this.createBetSlider();
    this.createCardZone();
    this.updateCards();
    
    this.toggleInput(false);
    this.updateView();
  }

  public updateView = (): void => {
    log.debug('Updating hand view');
    this.updateStack();
    this.updateTurn();
    this.updateCards();
    this.updateInputs();
  }

  update() {
    this.checkTouchingCards();
  }

  private checkTouchingCards() {
    const previouslyTouchingCards = this.touchingCards;
    this.touchingCards = this.checkPointer(this.game.input.mousePointer)
     || this.checkPointer(this.game.input.pointer1) 
     || this.checkPointer(this.game.input.pointer2);

    if (this.touchingCards !== previouslyTouchingCards) {
       this.updateCards();
     }    
  }

  private checkPointer(pointer: Phaser.Pointer): boolean {
    return pointer.isDown && this.cardZone.getBounds().contains(pointer.x, pointer.y);
  }

  private onClick(pointer: Phaser.Pointer): void {
      log.debug(`Clicked (${pointer.x}, ${pointer.y})`);
  }

  private updateStack() {
    const player = this.playerStateSupplier.getState();
    const amount = player ? player.playerState.stack : 0;
    this.stackAmountText.text = amount + '';
  }

  private updateTurn() {
    const previouslyActive = this.active;
    const bettingState = this.getBettingPlayerState();
    const playerState = this.getPlayerState();
    this.active = bettingState && bettingState.active;

    this.toggleInput(this.active);
    if (this.active && previouslyActive !== this.active) {
      navigator.vibrate(500);
    }
  }

  private updateCards() {
    log.debug('Updating cards');
    this.clearCards();
    const handPlayer = this.getHandPlayerState();
    if (!handPlayer) {
      return;
    }

    const cardOne = handPlayer.playerHand.cards[0];
    const cardTwo = handPlayer.playerHand.cards[1];

    this.cardOneImage = this.displayCard(cardOne, this.width() / 4);
    this.cardTwoImage = this.displayCard(cardTwo, this.width() * 3 / 4);
  }

  private updateInputs() {
    const bettingPlayer = this.getBettingPlayerState();
    if (!bettingPlayer) {
      return;
    }

    let buttonText = bettingPlayer.canCheck ? 'CHECK' : 'CALL';
    (this.checkCallButton.getChildAt(0) as Phaser.Text).text = buttonText;
  }

  private onClickCheckCall() {
    const bettingPlayer = this.getBettingPlayerState();
    if (!bettingPlayer) {
      return;
    }

    if (bettingPlayer.canCheck) {
      this.inputHandler.onClickCheck();
    } else {
      this.inputHandler.onClickCall();
    }
  }

  private clearCards() {
    if (this.cardOneImage) {
      this.cardOneImage.destroy();
    }
    if (this.cardTwoImage) {
      this.cardTwoImage.destroy();
    }
  }

  private toggleInput(enabled: boolean) {
    this.foldButton.inputEnabled = enabled;
    this.checkCallButton.inputEnabled = enabled;
    this.betRaiseButton.inputEnabled = enabled;
    this.betSliderKnob.inputEnabled = enabled;
    this.betSliderKnob.position.set(this.betSliderKnobMinX, this.betSliderKnob.y);

    let tint = enabled ? Phaser.Color.WHITE : 0x555555;
    this.inputs.forEach(input => input.tint = tint);
  }

  private createCardZone() {
    this.cardZone = this.game.add.graphics(0, this.height() / 2);
    // this.cardZone.lineStyle(3, Phaser.Color.WHITE);
    // this.cardZone.beginFill(Phaser.Color.WHITE, 0);
    this.cardZone.drawRect(0, this.betSliderKnob.height * 3, this.width(), this.height());
    this.cardZone.inputEnabled = true;
  }

  private onCardZoneEvent() {
    // this.cardsFaceUp = this.cardZone.input.pointerDown();
    this.updateCards();
  }

  private displayCard(card: Card, x: number): Phaser.Image | Phaser.Sprite | undefined {
    if (!card) {
      return;
    }

    const y = 3 * this.height() / 4;
    let cardImage;
    if (this.touchingCards) {
      cardImage = this.game.add.sprite(x, y, Assets.Spritesheets.SpritesheetsCards167220.getName(), getFrame(card));
    } else {
      cardImage = this.game.add.image(x, y, Assets.Images.ImagesCardBack.getName());
    }
    cardImage.anchor.setTo(0.5);
    cardImage.scale.set(3);
    return cardImage;
  }

  private createStackText() {
    const y = this.height() / 6;
    this.stackText = this.game.add.text(3 * this.width() / 8, y, 'Stack:', {
      font: DEFAULT_FONT,
      fill: 'white',
    });
    this.stackText.anchor.set(0.5);

    this.stackAmountText = this.game.add.text(6 * this.width() / 8, y, '0', {
      font: DEFAULT_FONT,
      align: 'right',
      fill: 'white',
    });
    this.stackAmountText.anchor.set(0.5);
    log.debug(`Width: ${this.width()}`);
  }

  private createFoldButton() {
    const x = this.width() / 6;
    this.foldButton = this.addButton(x, () => this.inputHandler.onClickFold(), 'FOLD');
  }

  private createCheckCallButton() {
    const x = this.width() / 2;
    this.checkCallButton = this.addButton(x, () => this.onClickCheckCall(), 'CHECK');
  }

  private createBetRaiseButton() {
    const x = 5 * this.width() / 6;
    this.betRaiseButton = this.addButton(x, () => this.inputHandler.onClickBet(Number.parseInt(this.betAmountText.text)), 'BET');
  }

  private createBetSlider() {
    const x = this.width() / 2 - 100;
    const y = this.height() / 2;
    this.betSliderBack = this.game.add.image(x, y, Assets.Images.ImagesSliderBack.getName());
    this.betSliderBack.anchor.set(0.5);

    this.betSliderKnob = this.game.add.image(0, 0, Assets.Images.ImagesSliderKnob.getName());
    this.betSliderKnob.anchor.set(0.5);
    this.betSliderBack.addChild(this.betSliderKnob);
    this.betSliderKnobMinX = -this.betSliderBack.width / 2 + this.betSliderKnob.width / 2 - 10;
    this.betSliderKnobMaxX = this.betSliderBack.width / 2 - this.betSliderKnob.width / 2 + 10;
    this.betSliderKnob.position.set(this.betSliderKnobMinX, 0);
    
    this.betSliderKnob.inputEnabled = true;
    this.betSliderKnob.input.useHandCursor = true;
    this.betSliderKnob.input.enableDrag();
    this.betSliderKnob.input.setDragLock(true, false);
    this.betSliderKnob.events.onDragUpdate.add(this.betSliderDragUpdate, this);
    this.inputs.push(this.betSliderKnob);
    this.inputs.push(this.betSliderBack);

    this.betAmountText = this.game.add.text(this.width() - 100, y, '0', {
      font: BET_AMOUNT_FONT,
      fill: 'white',
      boundsAlignH: 'right',
    });
    this.betAmountText.anchor.set(0.5);
    this.inputs.push(this.betAmountText);
  }

  private betSliderDragUpdate() {
    if (this.betSliderKnob.x > this.betSliderKnobMaxX) {
      this.betSliderKnob.position.set(this.betSliderKnobMaxX, this.betSliderKnob.y);
    }
    if (this.betSliderKnob.x < this.betSliderKnobMinX) {
      this.betSliderKnob.position.set(this.betSliderKnobMinX, this.betSliderKnob.y);
    }

    const playerState = this.playerStateSupplier.getState();
    const stackSize = playerState ? playerState.playerState.stack : 0;
    const percentage = stackSize * (this.betSliderKnob.x - this.betSliderKnobMinX) / (this.betSliderKnobMaxX - this.betSliderKnobMinX);
    this.betAmountText.text = percentage.toFixed(0);
  }

  private addButton(x: number, callback: Function, text: string): Phaser.Button {
    const y = 3 * this.height() / 8;
    const button = this.game.add.button(
      x, y, Assets.Atlases.AtlasesButton.getName(),
      callback, this, 1, 2, 0);
    button.anchor.set(0.5);
    button.scale.set(1.5);

    const buttonText = this.game.add.text(0, 0, text, {
      font: BUTTON_FONT
    });
    buttonText.anchor.set(0.5);
    button.addChild(buttonText);
    this.inputs.push(button);
    return button;
  }

  private getBettingPlayerState(): BettingPlayerState | undefined {
    const fullState = this.playerStateSupplier.getState();
    if (fullState) {
      return fullState.bettingPlayerState;
    }

    return undefined;
  }

  private getHandPlayerState(): HandPlayerState | undefined {
    const fullState = this.playerStateSupplier.getState();
    if (fullState) {
      return fullState.handPlayerState;
    }

    return undefined;
  }

  private getHandState(): HandState | undefined {
    const fullState = this.playerStateSupplier.getState();
    if (fullState) {
      return fullState.handState;
    }

    return undefined;
  }

  private getPlayerState(): PlayerState | undefined {
    const fullState = this.playerStateSupplier.getState();
    if (fullState) {
      return fullState.playerState;
    }

    return undefined;
  }

  private width(): number {
    return this.game.world.width;
  }

  private height(): number {
    return this.game.world.height;
  }

}