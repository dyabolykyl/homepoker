import * as Assets from '../../assets';

import { BaseState } from '../../states/base';
import { ChatRequest } from '../../common/socket/Request';
import { Context } from '../../utils/context';
import { InputField } from '../../phaser-input/InputField';
import { PhaserInput } from '../../phaser-input/Plugin';
import { SessionManager } from '../../socket/SessionManager';
import { TableManager } from '../game/TableManager';
import { hasFailed } from '../../common/socket/Response';
import { log } from '../../utils/log';

export class ChatState extends BaseState {

  private tableManager: TableManager;

  private chatButton: Phaser.Button;
  private chatInput: InputField;

  public init(context: Context) {
    super.init(context);
    this.tableManager = context.getComponent('TableManager');
  }

  public create(): void {
    log.debug(`TableState created`);
    this.game.stage.setBackgroundColor('#000C26');

    this.chatButton = this.game.add.button(
      this.centerX, 
      this.centerY - 500, 
      Assets.Atlases.AtlasesButton.getName(), 
      this.sendChat, this, 1, 2, 0);
    this.chatButton.anchor.setTo(0.5);
    this.chatButton.scale.setTo(2);
    const chatButtonText = this.game.add.text(0, 0, 'Chat!');
    chatButtonText.anchor.setTo(0.5);
    this.chatButton.addChild(chatButtonText);

    const inputWidth = 1000;
    const inputHeight = 100;
    const inputX = this.centerX - inputWidth / 2;
    const inputY = this.centerY - innerHeight / 2;
    const inputOptions = {
      width: inputWidth,
      height: inputHeight,
      font: '80px Arial',
      placeHolder: 'Message...',
      padding: 10,
      zoom: false
    };
    this.chatInput = (this.game as PhaserInput.InputFieldGame).add.inputField(inputX, inputY, inputOptions); 
  }

  private sendChat = async (): Promise<void> => {
    const message = this.chatInput.value;
    this.chatButton.inputEnabled = false;
    
    const response = await this.tableManager.chat(message);
    if (hasFailed(response)) {
      log.warn(`Chat request failed`);
    }
    this.chatButton.inputEnabled = true;
    this.chatInput.resetText();
  }
}
