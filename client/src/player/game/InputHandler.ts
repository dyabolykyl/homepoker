import { EventBus, EventType } from '../../common/utils/Emitter';
import { TableJoinedEvent, TableManager } from './TableManager';

import { ActionType } from '../../common/game/PlayerAction';
import { PlayerActionRequest } from '../../common/socket/Request';
import { SessionManager } from '../../socket/SessionManager';

export interface InputHandler {
  onClickFold();
  onClickCheck();
  onClickCall();
  onClickBet(amount: number);
}

export class InputHandlerImpl implements InputHandler {

  private readonly sessionManager: SessionManager;
  private readonly tableManager: TableManager;

  constructor(sessionManager: SessionManager, tableManager: TableManager) { 
    this.sessionManager = sessionManager;
    this.tableManager = tableManager;
  }

  onClickFold() {
    this.sessionManager.send(this.tableManager.makePlayerActionRequest(ActionType.FOLD));
  }

  onClickCheck() {
    this.sessionManager.send(this.tableManager.makePlayerActionRequest(ActionType.CHECK));
  }

  onClickCall() {
    this.sessionManager.send(this.tableManager.makePlayerActionRequest(ActionType.CALL));
  }
  
  onClickBet(amount: number) {
    this.sessionManager.send(this.tableManager.makePlayerActionRequest(ActionType.BET, amount));
  }
}