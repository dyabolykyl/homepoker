import { ChatRequest, PlayerActionRequest, SocketEvent, UpdatePlayerStateRequest, makeJoinTableRequest } from '../../common/socket/Request';
import { Event, EventBus, EventType } from '../../common/utils/Emitter';
import { INVALID_REQUEST, JoinTableResponse, Response, SUCCESS, hasFailed } from '../../common/socket/Response';

import { ActionType } from '../../common/game/PlayerAction';
import { FullPlayerState } from '../../common/game/TableState';
import { Lifecycle } from '../../common/utils/Lifecycle';
import { MainMenuView } from '../view/MainMenuView';
import { PlayerStateSupplier } from '../states/hand';
import { SessionManager } from '../../socket/SessionManager';
import { log } from '../../utils/log';

export class TableJoinedEvent implements Event {
  eventType = EventType.TABLE_JOINED;
  readonly tableId: string;

  constructor(tableId: string) {
    this.tableId = tableId;
  }
}

export class TableManager implements Lifecycle, PlayerStateSupplier {

  mainMenuView: MainMenuView;

  private readonly sessionManager: SessionManager;
  private readonly eventBus: EventBus;

  private currentlyJoinedTable: string;
  private playerState: FullPlayerState;

  constructor(sessionManager: SessionManager,
              eventBus: EventBus) {
    this.sessionManager = sessionManager;
    this.eventBus = eventBus;
  }

  start() {
    log.debug('Starting');
    this.sessionManager.listen(SocketEvent.UPDATE_PLAYER_STATE, this.onUpdatePlayerState);
  }

  stop() {
    
  }

  getState(): FullPlayerState | undefined {
    return this.playerState;
  }

  async joinTable(tableId: string, playerName: string) {
    log.info(`Attempting to join table ${tableId} as ${playerName}`);
    try {
      const res: JoinTableResponse = await this.sessionManager.send<JoinTableResponse>(makeJoinTableRequest(tableId, playerName));
      if (!hasFailed(res)) {
        log.info(`Successfully joined table!`);
        this.currentlyJoinedTable = tableId;
        if (this.mainMenuView) {
          this.mainMenuView.showGameScreen();
        }
        this.eventBus.post(new TableJoinedEvent(tableId));
        this.updatePlayerState(res.playerState);
      }
    } catch (err) {
      log.warn(`Failed to join table: ${err}`);
    }
  }

  async chat(message: string): Promise<Response> {
    if (!this.currentlyJoinedTable) {
      return INVALID_REQUEST;
    }

    if (!message || message === '') {
      log.warn('Missing chat message');
      return INVALID_REQUEST;
    }

    const request = new ChatRequest(this.currentlyJoinedTable, message);
    return this.sessionManager.send(request);    
  }

  makePlayerActionRequest(actionType: ActionType, amount?: number): PlayerActionRequest {
    return new PlayerActionRequest(this.currentlyJoinedTable, {
      id: this.sessionManager.getUuid(),
      actionType,
      amount
    });
  }

  private onUpdatePlayerState = (req: UpdatePlayerStateRequest): Response => {
    if (!req) {
      log.warn(`Empty request`);
      return INVALID_REQUEST;
    }

    if (!req.fullPlayerState || !req.fullPlayerState.playerState) {
      log.warn(`Missing player state`);
      return INVALID_REQUEST;
    }

    if (req.fullPlayerState.playerState.id !== this.sessionManager.getUuid()) {
      log.warn('Received request for unknown player');
      return INVALID_REQUEST;
    }

    this.updatePlayerState(req.fullPlayerState);
    return SUCCESS;
  }

  private updatePlayerState(state: FullPlayerState) {
    this.playerState = state;
    this.eventBus.post({ eventType: EventType.PLAYER_STATE_UPDATED });
  }
}
