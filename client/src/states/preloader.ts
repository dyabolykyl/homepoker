import * as AssetUtils from '../utils/assetUtils';
import * as Assets from '../assets';

import { EventBus, SyncEventBus } from '../common/utils/Emitter';
import { FakeSocketAdapter, SocketAdapter, SocketIoSocketAdapter } from '../socket/SocketAdapter';
import { PlayerManager, PlayerManagerImpl } from '../host/connection/PlayerManager';
import { SessionManager, SessionManagerImpl } from '../socket/SessionManager';
import { TableRunner, TableRunnerImpl } from '../host/game/TableRunner';

import { ChatState } from '../player/states/chat';
import { Context } from '../utils/context';
import { FakeGameServer } from '../host/fakes/FakeGameServer';
import HostMain from '../host/states/main';
import { InputHandlerImpl } from '../player/game/InputHandler';
import { MobileHandState } from '../player/states/hand';
import PlayerMain from '../player/states/main';
import { PokerEvaluator } from '../host/game/PokerEvaluator';
import { TableManager } from '../player/game/TableManager';
import { TableStateSupplier } from '../host/game/TableStateSupplier';

export default class Preloader extends Phaser.State {
    private preloadBarSprite: Phaser.Sprite;
    private preloadFrameSprite: Phaser.Sprite;
    private context: Context;

    public preload(): void {
        this.game.stage.setBackgroundColor('#000000');
        // Setup your loading screen and preload sprite (if you want a loading progress indicator) here

        this.preloadBarSprite = this.game.add.sprite(
            this.game.world.centerX,
            this.game.world.centerY,
            Assets.Atlases.AtlasesPreloadSpritesArray.getName(),
            Assets.Atlases.AtlasesPreloadSpritesArray.Frames.PreloadBar);
        this.preloadBarSprite.anchor.setTo(0, 0.5);
        this.preloadBarSprite.x -= this.preloadBarSprite.width * 0.5;

        this.preloadFrameSprite = this.game.add.sprite(
            this.game.world.centerX,
            this.game.world.centerY,
            Assets.Atlases.AtlasesPreloadSpritesArray.getName(),
            Assets.Atlases.AtlasesPreloadSpritesArray.Frames.PreloadFrame);
        this.preloadFrameSprite.anchor.setTo(0.5);

        this.game.load.setPreloadSprite(this.preloadBarSprite);

        this.context = new Context();

        const eventBus: EventBus = this.context.addComponent('EventBus', new SyncEventBus);

        let socketAdapter: SocketAdapter;
        if (FAKE) {
            const fakeGameServer: FakeGameServer = this.context.addComponent('FakeGameServer', new FakeGameServer(eventBus));
            socketAdapter = this.context.addComponent('SocketAdapter', new FakeSocketAdapter(fakeGameServer));
        } else {
            socketAdapter = this.context.addComponent('SocketAdapter', new SocketIoSocketAdapter());
        }
        if (this.game.device.desktop && !MOBILE) {
            this.game.state.add('main', HostMain);
            const pokerEvaluator: PokerEvaluator = this.context.addComponent('PokerEvaluator', new PokerEvaluator());
            const tableStateSupplier: TableStateSupplier = this.context.addComponent('TableStateSupplier', new TableStateSupplier());
            const sessionManager: SessionManager =
                this.context.addComponent('SessionManager', new SessionManagerImpl(socketAdapter));
            const playerManager: PlayerManager =
                this.context.addComponent('PlayerManager', new PlayerManagerImpl(sessionManager, tableStateSupplier));
            const tableRunner: TableRunner =
                this.context.addComponent('TableRunner', new TableRunnerImpl(sessionManager, tableStateSupplier, eventBus));
            playerManager.setTableRunner(tableRunner);
        } else {
            this.game.state.add('main', PlayerMain);
            this.game.state.add('chat', ChatState);
            this.game.state.add('hand', MobileHandState);
            const sessionManager: SessionManager =
                this.context.addComponent('SessionManager', new SessionManagerImpl(socketAdapter));
            const tableManager = this.context.addComponent('TableManager', new TableManager(sessionManager, eventBus));
            this.context.addComponent('InputHandler', new InputHandlerImpl(sessionManager, tableManager));
        }

        this.context.start();
        AssetUtils.Loader.loadAllAssets(this.game, this.waitForSoundDecoding, this);
    }

    private waitForSoundDecoding(): void {
        AssetUtils.Loader.waitForSoundDecoding(this.startGame, this);
    }

    private startGame(): void {
        this.game.camera.onFadeComplete.addOnce(this.loadTitle, this);
        this.game.camera.fade(0x000000, 1000);
    }

    private loadTitle(): void {
        this.game.state.start('main', undefined, undefined, this.context);
    }
}
