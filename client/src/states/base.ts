import { Context } from '../utils/context';

export interface TextInput extends Phaser.Sprite {
  // canvasInput: Phaser.Input.CanvasInput;
}

export class BaseState extends Phaser.State {
  context: Context;
  protected centerX: number;
  protected centerY: number;

  init(context: Context) {
    this.context = context;
    this.centerX = this.game.world.centerX;
    this.centerY = this.game.world.centerY;
  }
  
  protected createInput(x: number, y: number) {
    const bmd = this.add.bitmapData(400, 50);    
    const myInput = this.game.add.sprite(x, y, bmd);
    
    return myInput;
  }
}
