import { FullPlayerState, TableState } from '../game/TableState';

import { PlayerAction } from '../game/PlayerAction';

export enum SocketEvent {
  SET_UUID = 'set_uuid',

  HOST_TABLE = 'host_game',
  VIEW_TABLE = 'view_game',
  UPDATE_VIEW = 'update_view',
  UPDATE_PLAYER_STATE = 'update_player_state',

  JOIN_TABLE = 'join_game',
  CHAT = 'chat',
  PLAYER_ACTION = 'player_action'
}

export interface Request {
  event: SocketEvent;
  uuid?: string;
}

export interface TableRequest extends Request {
  tableId: string;
}

export function makeSetUuidRequest(uuid: string): Request {
  return {
    event: SocketEvent.SET_UUID,
    uuid
  };
}

export interface ChatRequest extends TableRequest {uuid: string; }
export class ChatRequest {
  event = SocketEvent.CHAT;
  message: string;

  constructor(tableId: string, message: string) {
    this.tableId = tableId;
    this.message = message;
  }
}

export interface HostTableRequest extends TableRequest {
  
}

export function makeHostTableRequest(tableId: string): HostTableRequest {
  return {
    event: SocketEvent.HOST_TABLE,
    tableId
  };
}

export interface ViewTableRequest extends TableRequest { }
export class ViewTableRequest implements TableRequest {
  event = SocketEvent.VIEW_TABLE;

  constructor(tableId: string) {
    this.tableId = tableId;
  }
}

export interface UpdateViewRequest extends TableRequest {
  tableState: TableState;
 }
export class UpdateViewRequest implements TableRequest {
  event = SocketEvent.UPDATE_VIEW;

  constructor(tableId: string, tableState: TableState) {
    this.tableId = tableId;
    this.tableState = tableState;
  }
}

export interface JoinTableRequest extends TableRequest {
  playerName: string;
}

export function makeJoinTableRequest(tableId: string, playerName: string): JoinTableRequest {
  return {
    event: SocketEvent.JOIN_TABLE,
    tableId,
    playerName
  };
}

export interface PlayerActionRequest extends TableRequest {
  action: PlayerAction;
 }
export class PlayerActionRequest implements TableRequest {
  event = SocketEvent.PLAYER_ACTION;

  constructor(tableId: string, action: PlayerAction) {
    this.tableId = tableId;
    this.action = action;
  }
}

export interface UpdatePlayerStateRequest extends TableRequest {
  fullPlayerState: FullPlayerState;
}
export class UpdatePlayerStateRequest implements TableRequest {
  event = SocketEvent.UPDATE_PLAYER_STATE;

  constructor(tableId: string, fullPlayerState: FullPlayerState) {
    this.tableId = tableId;
    this.fullPlayerState = fullPlayerState;
  }
}
