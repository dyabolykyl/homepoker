import { FullPlayerState, TableState } from '../game/TableState';

import { Request } from './Request';

export enum Result {
  SUCCESS = 'success',
  TIMEOUT = 'timeout',
  CONNECTION_ERROR = 'connection_error',
  INVALID_REQUEST = 'invalid_request',
  INVALID_TABLE = 'invalid_table',
}

export interface Response {
  result: Result;
}

export const CONNECTION_ERROR: Response = {
  result: Result.CONNECTION_ERROR
};

export const INVALID_REQUEST: Response = {
  result: Result.INVALID_REQUEST
};

export const SUCCESS: Response = {
  result: Result.SUCCESS
};

export const INVALID_TABLE: Response = {
  result: Result.INVALID_TABLE
};

export const TIMEOUT: Response = {
  result: Result.TIMEOUT
};

export type RequestHandler<R extends Request, T extends Response> = (req: R, callback: Callback<T>) => void;
export type Callback<T extends Response> = (res: T) => void;

export function hasFailed(res: Response) {
  return res.result !== Result.SUCCESS;
}

export interface ViewTableResponse extends Response { }
export class ViewTableResponse implements ViewTableResponse {  
 tableState: TableState;
 
 constructor(tableState: TableState) {
   this.result = Result.SUCCESS;
   this.tableState = tableState;
 }
}

export interface JoinTableResponse extends Response { }
export class JoinTableResponse implements JoinTableResponse {
  playerState: FullPlayerState;

  constructor(playerState: FullPlayerState) {
    this.result = Result.SUCCESS;
    this.playerState = playerState;
  }
}
