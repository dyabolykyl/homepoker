
export interface PlayerState {
  readonly id: string;
  readonly name: string;
  seat: number;
  stack: number;
  isHost?: boolean;
  chatMessage?: string;
}
