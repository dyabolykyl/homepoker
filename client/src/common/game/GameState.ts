import { EMPTY_HAND, HandState } from './HandState';

export interface GameState {
  readonly running: boolean;
  readonly handState: HandState;
}

export const EMPTY_GAME: GameState = {
  running: false,
  handState: EMPTY_HAND
};
