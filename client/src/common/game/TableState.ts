import { EMPTY_GAME, GameState } from '../game/GameState';

import { BettingPlayerState } from './BettingState';
import { HandPlayerState } from '../models/HandPlayer';
import { HandState } from './HandState';
import { PlayerState } from './PlayerState';

export interface FullPlayerState {
  readonly playerState: PlayerState;
  readonly handState?: HandState;
  readonly handPlayerState?: HandPlayerState;
  readonly bettingPlayerState?: BettingPlayerState;
}

export class TableState {
  tableId: string;
  readonly playerStates: PlayerState[] = [];
  host: string;
  gameState: GameState = EMPTY_GAME;

  addPlayerState(playerState: PlayerState): void {
    if (!this.getPlayer(playerState.id)) {
      this.playerStates.push(playerState);
    }
  }

  removePlayer(playerId: string): void {
    const index = this.playerStates.findIndex(p => p.id === playerId);
    if (index >= 0) {
      this.playerStates.splice(index, 1);
    }
  }

  getPlayer(playerId: string | undefined): PlayerState | undefined {
    if (!playerId) {
      return undefined;
    }

    return this.playerStates.find(p => p.id === playerId);
  }

  getFullPlayerState(playerId: string): FullPlayerState;
  getFullPlayerState(playerId: string, secureHandState: HandState): FullPlayerState;
  getFullPlayerState(playerId: string, secureHandState?: HandState): FullPlayerState {
    secureHandState = secureHandState || this.getSecureHandState();
    const playerState = this.getPlayer(playerId) as PlayerState;
    return {
      playerState,
      handState: secureHandState,
      handPlayerState: this.gameState.handState.handPlayers.find(p => p.id === playerState.id) as HandPlayerState,
      bettingPlayerState: secureHandState.bettingState.bettingPlayerStates.find(p => p.id === playerState.id) as BettingPlayerState
    };
  }

  getFullPlayerStates(): FullPlayerState[] {
    const secureHandState = this.getSecureHandState();
    return this.playerStates.map(p => this.getFullPlayerState(p.id, secureHandState));
  }

  getSecureHandState(): HandState {
    return {
      ...this.gameState.handState,
      handPlayers: []
    };
  }

  setGameState(gameState: GameState): void {
    this.gameState = gameState;
  }
}
