
export interface BettingPlayerState {
  readonly id: string;
  readonly active: boolean;
  readonly activeBet: number;
  readonly canCheck: boolean;
}

export interface BettingState {
  readonly bettingPlayerStates: BettingPlayerState[];
}

export const EMPTY_BETTING_STATE: BettingState = {
  bettingPlayerStates: []
};
