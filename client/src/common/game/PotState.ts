
export interface PotState {
  readonly subPots: number[];
}

export const EMPTY_POT: PotState = {
  subPots: [],
};
