import { BettingState, EMPTY_BETTING_STATE } from './BettingState';
import { EMPTY_POT, PotState } from './PotState';

import { Card } from '../models/Card';
import { HandPlayerState } from '../models/HandPlayer';
import { Suit } from '../models/Suit';
import { Value } from '../models/Value';

export interface HandState {
  readonly handId: number;
  readonly board: Card[];
  readonly handPlayers: HandPlayerState[];
  readonly cardsFaceUp: boolean;
  readonly potState: PotState;
  readonly bettingState: BettingState;
  readonly statusMessage?: string;
}

export const EMPTY_HAND: HandState = {
  handId: 0,
  board: [],
  handPlayers: [],
  potState: EMPTY_POT,
  cardsFaceUp: false,
  bettingState: EMPTY_BETTING_STATE,
};

export const FAKE_BOARD = [
  new Card(Suit.SPADE, Value.EIGHT), 
  new Card(Suit.HEART, Value.KING),
  new Card(Suit.SPADE, Value.TWO),
  new Card(Suit.SPADE, Value.TWO),
  new Card(Suit.SPADE, Value.TWO)
];
