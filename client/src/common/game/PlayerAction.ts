
export enum ActionType {
  FOLD = 'fold',
  CHECK = 'check',
  CALL = 'call',
  BET = 'bet'
}

export interface PlayerAction {
  readonly id: string;
  readonly actionType: ActionType;
  readonly amount?: number;
}
