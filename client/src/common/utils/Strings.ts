
export class ToStringHelper {
  private readonly title: string;
  private readonly attributes: string[][] = [];

  constructor(title: string) {
    this.title = title;
  }

  addValue(value: any): ToStringHelper {
    return this.add(typeof value, value);
  }

  add(key: string, value: any): ToStringHelper {
    const attribute = value === undefined ? 'undefined' : JSON.stringify(value);
    this.attributes.push([
      key,
      attribute
    ]);
    return this;
  }

  toString(): string {
    return `${this.title}{${this.attributes.map(pair => `${pair[0]}=${pair[1]}`).join(' ')}}`;
  }
}

export function toStringHelper(name: string): ToStringHelper {
  return new ToStringHelper(name);
}
