
export interface Lifecycle {
  start(): void | Promise<void>;
  stop(): void | Promise<void>;
}