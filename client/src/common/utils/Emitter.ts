import { Executor, TimedExecutor } from './Executor';

import { Multimap } from './Multimap';
import { log } from '../../utils/log';

export enum EventType {
  // host events
  HOST_TABLE_ID_SET = 'host_table_id_set',
  GAME_STARTED = 'game_started',

  // player events
  PLAYER_JOIN_GAME_CLICKED = 'player_join_table_clicked',
  TABLE_JOINED = 'table_joined',
  PLAYER_STATE_UPDATED  = 'player_state_updated'
}

export interface Event {
  eventType: EventType;
}

export class Emitter {

  private readonly listeners: Multimap<string, Function> = new Multimap();

  protected on(event: string, listener: any): void {
    this.listeners.put(event, listener);
  }

  protected emit(event: string, ...args: any[]): void {
    this.listeners.getAll(event).forEach(listener => {
      listener(...args);
    });
  }

}

export abstract class EventBus extends Emitter {

  abstract subscribe<T extends Event>(eventType: EventType, callback: (event: T) => void): void;

  abstract post<T extends Event>(event: T): void;

}

export class SyncEventBus extends EventBus {

  subscribe<T extends Event>(eventType: EventType, callback: (event: T) => void): void {
    log.debug(`Subscribing to ${eventType} event`);
    this.on(eventType, callback);
  }

  post<T extends Event>(event: T): void {
    log.debug(`Posting ${JSON.stringify(event)} event`);
    this.emit(event.eventType, event);
  }

}

export class AsyncEventBus extends SyncEventBus {

  private readonly executor = new TimedExecutor();

  post<T extends Event>(event: T): void {
    this.executor.submit(() => {
      log.debug(`Posting ${JSON.stringify(event)} event`);
      this.emit(event.eventType, event);
    });
  }

}
