import { Deck } from '../models/Deck';
import { checkState } from '../../utils/preconditions';
import { log } from '../../utils/log';

export interface Executor {
  submit(task: () => void, delay?: number);
}

export class TimedExecutor implements Executor {

  submit(task: () => void, delay?: number) {
    setTimeout(task, delay || 0);
  }
}

interface FutureTask {
  task: () => void;
  timeToExecute: number;
}

export class DeterministicExecutor implements Executor {

  private currentTime = 0;
  private tasks: FutureTask[] = [];

  submit(task: () => void, delay?: number) {
    delay = delay || 0;
    this.tasks.push({ task, timeToExecute: this.currentTime + delay });
    // log.debug(`Submitted task to run at ${delay + this.currentTime}. Now: ${this.currentTime}. Total tasks: ${this.tasks.length}`);
    this.executeCurrentTasks();
  }

  tick(time: number) {
    checkState(time >= 0, 'cannot tick backwards');
    // log.debug(`Moving time from ${this.currentTime} to ${this.currentTime + time}`);
    this.currentTime += time;
    this.executeCurrentTasks();
  }

  private executeCurrentTasks() {
    const tasksToExecute: FutureTask[] = [];
    const remainingTasks = this.tasks.filter(futureTask => {
      if (this.currentTime >= futureTask.timeToExecute) {
        tasksToExecute.push(futureTask);
        return false;
      }
      return true;
    });
    // log.debug(`Tasks removed: ${this.tasks.length - remainingTasks.length}`);
    this.tasks = remainingTasks;
    tasksToExecute.forEach(f => f.task());
  }

}