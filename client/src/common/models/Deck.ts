import { Card } from './Card';
import { EnumValues } from 'enum-values';
import { Suit } from './Suit';
import { Value } from './Value';
import { checkState } from '../../utils/preconditions';

export class Deck {
  private readonly cards: Card[];

  constructor() {
    this.cards = [];
    EnumValues.getValues(Suit).forEach((suit: Suit) => {
      EnumValues.getValues(Value).forEach((value: Value) => {
        this.cards.push(new Card(suit, value));
      });
    });

    this.cards = this.shuffle(this.cards);
  }

  public draw(): Card {
    checkState(this.cards.length > 0, 'no more cards in deck');
    return this.cards.pop() as Card;
  }

  public drawMany(amount: number): Card[] {
    const drawn = [];
    for (let i = 0; i < amount; i++) {
      drawn.push(this.cards.pop() as Card);    
    }
    return drawn;
  }

  private shuffle(cards: Card[]) {
    var currentIndex = cards.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = cards[currentIndex];
      cards[currentIndex] = cards[randomIndex];
      cards[randomIndex] = temporaryValue;
    }
  
    return cards;
  }
}