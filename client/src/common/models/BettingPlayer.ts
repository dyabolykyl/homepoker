import { HandPlayer } from './HandPlayer';
import { ToStringHelper } from '../utils/Strings';

export class BettingPlayer {
  readonly handPlayer: HandPlayer;
  totalBet: number;
  considered: boolean;

  constructor(handPlayer: HandPlayer) {
    this.handPlayer = handPlayer;
    this.totalBet = 0;
    this.considered = false;
  }

  public takeBet(bet: number): number {
    const amount = this.handPlayer.takeBet(bet);
    this.totalBet += amount;
    return amount;
  }

  public toString(): string {
    return new ToStringHelper('BettingPlayer')
      .addValue(this.handPlayer)
      .add('totalBet', this.totalBet)
      .toString();
  }
}
