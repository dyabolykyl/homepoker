import { PlayerState } from '../game/PlayerState';
import { toStringHelper } from '../utils/Strings';

export class Player {
  readonly state: PlayerState;

  constructor(id: string, name: string, seat: number, stack?: number) {
    this.state = {
      id,
      name, 
      stack: stack || 0,
      seat,
    };
  }

  isActive(): boolean {
    return this.state.stack > 0;
  }

  adjustStack(amount: number): number {
    if (amount > 0) {
      this.state.stack += amount;
      return amount;
    }

    amount = amount * -1;
    if (this.state.stack < amount) {
      amount = this.state.stack;
    }

    this.state.stack -= amount;
    return amount;
  }

  toString(): string {
    return toStringHelper('Player')
      .addValue(JSON.stringify(this.state))
      .toString();
  }
}
