import { Suit } from './Suit';
import { Value } from './Value';
import { toStringHelper } from '../utils/Strings';

export class Card {
  readonly value: Value;
  readonly suit: Suit;
  
  constructor(suit: Suit, value: Value) {
    this.suit = suit;
    this.value = value;
  }

  toString = (): string => {
    return toStringHelper('Card')
      .add('suit', this.suit)
      .add('value', this.value)
      .toString();
  }
}