
export interface GameOptions {
  smallBlind: number;
  bigBlind: number;
}
