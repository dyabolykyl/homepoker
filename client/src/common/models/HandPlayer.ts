import { Player } from './Player';
import { PlayerHand } from './PlayerHand';
import { toStringHelper } from '../utils/Strings';

export interface HandPlayerState {
  readonly id: string;
  readonly playerHand: PlayerHand;
  readonly status: HandPlayerStatus;
  // readonly position: number;
}

export enum HandPlayerStatus {
  IN = 'in',
  ALL_IN = 'all_in',
  OUT = 'out'
}

export class HandPlayer {
  readonly player: Player;
  readonly id: string;
  readonly playerHand: PlayerHand;
  readonly initialStack: number;
  status: HandPlayerStatus;
  totalBet: number;
  // readonly position: number;

  constructor(player: Player) { // , position: number) {
    this.player = player;
    this.id = player.state.id;
    this.playerHand = new PlayerHand();
    this.status = HandPlayerStatus.IN;
    this.totalBet = 0;
    this.initialStack = player.state.stack;
  }

  public getState(): HandPlayerState {
    return {
      id: this.player.state.id,
      playerHand: this.playerHand,
      status: this.status
    };
  }

  public takeBet(bet: number): number {
    const amount = this.player.adjustStack(-bet);
    if (this.player.state.stack === 0) {
      this.status = HandPlayerStatus.ALL_IN;
    }
    this.totalBet += amount;
    return amount;
  }

  public sitOut(): void {
    this.status = HandPlayerStatus.OUT;
  }

  public toString(): string {
    return toStringHelper('HandPlayer')
      .addValue(this.player)
      .addValue(this.playerHand)
      .add('status', status)
      .toString();
  }
}
