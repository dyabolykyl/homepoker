import { Card } from './Card';
import { checkState } from '../../utils/preconditions';

const MAX_CARDS: number = 2;

export class PlayerHand {
  readonly cards: Card[];

  constructor(cards?: Card[]) {
    this.cards = cards || [];
    checkState(this.cards.length <= MAX_CARDS, 'max cards exceeded');
  }

  addCard = (card: Card): void => {
    checkState(this.cards.length < MAX_CARDS, 'already have max cards');
    this.cards.push(card);
  }

}