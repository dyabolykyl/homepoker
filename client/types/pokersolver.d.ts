
declare namespace pokersolver {
  
}

declare class Hand {
  static solve: (cards: string[]) => Hand;
  static winners: (hands: Hand[]) => Hand[];
  name: string;
  descr: string;
  cardPool: string[];
}
