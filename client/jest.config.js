module.exports = {
  "roots": [
    "<rootDir>/src",
    "<rootDir>/tst",
  ],
  "transform": {
    "^.+\\.ts$": "ts-jest"
  },
  "testRegex": ".*\\.test\\.ts$",
  "moduleFileExtensions": [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node"
  ],
}