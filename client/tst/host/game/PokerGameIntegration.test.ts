import * as logging from '../../../src/utils/log';

import { AUTOPLAY_DELAY_MILLIS, PokerGame } from '../../../src/host/game/PokerGame';
import { ActionType, PlayerAction } from '../../../src/common/game/PlayerAction';
import { BettingPlayerState, BettingState } from '../../../src/common/game/BettingState';
import { EMPTY_HAND, HandState } from '../../../src/common/game/HandState';

import { DeterministicExecutor } from '../../../src/common/utils/Executor';
import { GameOptions } from '../../../src/common/models/GameOptions';
import { Player } from '../../../src/common/models/Player';
import { checkState } from '../../../src/utils/preconditions';
import { emptyLog } from '../../../src/utils/log';

let nextIdentifier = 1;
let nextSeat = 0;
let game: PokerGame;
let gameOptions: GameOptions;
const INITIAL_STACK = 100;
let executor: DeterministicExecutor;

function disableLogging() {
  logging['log' as any] = emptyLog;
}

function enableLogging() {
  logging['log' as any] = console;
}

describe('poker tests', () => {
  let onStateUpdated: jest.Mock;

  beforeEach(() => {
    disableLogging();
    nextSeat = 0;
    onStateUpdated = jest.fn();
    gameOptions = {
      smallBlind: 1,
      bigBlind: 2
    };
    executor = new DeterministicExecutor();
    game = new PokerGame(onStateUpdated, executor);
  });

  test('Empty game', () => {
    const state = game.getState();
    expect(state.handState).toEqual(EMPTY_HAND);
  });

  test('Players joined but game not started', () => {
    game.addPlayer(arrangePlayer(1));
    game.addPlayer(arrangePlayer(2));
    game.addPlayer(arrangePlayer(3));
    expect(game.getState().handState).toEqual(EMPTY_HAND);
    expect(onStateUpdated).toHaveBeenCalledTimes(3);
  });

  test('Player joins with duplicate seat expect no action', () => {
    const p1 = arrangePlayer(1);
    const p2 = new Player('samestack', 'samestack', p1.state.seat, 100);
    game.addPlayer(p1);
    expect(onStateUpdated).toHaveBeenCalledTimes(1);
    game.addPlayer(p2);
    expect(onStateUpdated).toHaveBeenCalledTimes(1);
  });

  test('Two players, start game', () => {
    // enableLogging();
    const p1 = arrangePlayer(1);
    const p2 = arrangePlayer(2);
    game.addPlayer(p1);
    game.addPlayer(p2);

    game.start(gameOptions);

    expectHandId(1);
    expectPlayersInHand(p1, p2);
    expectCardsFaceDown();
    expectCurrentPlayer(p2);
    expectSubPots(3);
    expectStackChange(p1, -gameOptions.bigBlind);
    expectActiveBet(p1, gameOptions.bigBlind);
    expectStackChange(p2, -gameOptions.smallBlind);
    expectActiveBet(p2, gameOptions.smallBlind);
  });

  test('Three players, start game', () => {
    const p1 = arrangePlayer(1);
    const p2 = arrangePlayer(2);
    const p3 = arrangePlayer(3);
    game.addPlayer(p1);
    game.addPlayer(p2);
    game.addPlayer(p3);

    game.start(gameOptions);

    expectHandId(1);
    expectPlayersInHand(p1, p2, p3);
    expectPreflop();
    expectCardsFaceDown();
    expectCurrentPlayer(p2);
    expectSubPots(3);
    expectStackChange(p3, -gameOptions.smallBlind);
    expectActiveBet(p3, gameOptions.smallBlind);
    expectStackChange(p1, -gameOptions.bigBlind);
    expectActiveBet(p1, gameOptions.bigBlind);
    expectStackChange(p2, 0);
  });

  test('Two players, immediate fold', () => {
    // enableLogging();
    const p1 = arrangePlayer(1);
    const p2 = arrangePlayer(2);
    game.addPlayer(p1);
    game.addPlayer(p2);

    game.start(gameOptions);
    expect(arrangePlayerAction(p2, ActionType.FOLD)).toBeTruthy();
    expectCardsFaceDown();
    expectPreflop();
    expectSubPots(2);
    tick();

    expectHandId(2);
    expectPreflop();
    expectCurrentPlayer(p1);
    expectStackChange(p1, 0);
    expectStackChange(p2, -3);
    expectSubPots(3);
  });

  test('All in bet, fold', () => {
    // enableLogging();
    const p1 = arrangePlayer(1);
    const p2 = arrangePlayer(2);
    game.addPlayer(p1);
    game.addPlayer(p2);

    game.start(gameOptions);
    expect(arrangePlayerAction(p2, ActionType.BET, INITIAL_STACK - gameOptions.smallBlind)).toBeTruthy();
    expectCardsFaceDown();
    expectPreflop();
    expectSubPots(102);
    expectCurrentPlayer(p1);

    expect(arrangePlayerAction(p1, ActionType.FOLD)).toBeTruthy();
    expectCardsFaceDown();
    expectPreflop();
    expectSubPots(4);
    tick();

    expectHandId(2);
    expectPreflop();
    expectCurrentPlayer(p1);
    expectStackChange(p1, -3);
    expectStackChange(p2, 0);
    expectSubPots(3);
  });

  test('Bet, all in call, all in call', () => {
    // enableLogging();
    const p1 = arrangePlayer(1, 50);
    const p2 = arrangePlayer(2, 100);
    const p3 = arrangePlayer(3, 60);
    game.addPlayer(p1);
    game.addPlayer(p2);
    game.addPlayer(p3);

    game.start(gameOptions);
    expect(arrangePlayerAction(p2, ActionType.BET, p2.state.stack)).toBeTruthy();
    expectCardsFaceDown();
    expectPreflop();
    expectSubPots(103);
    expectCurrentPlayer(p3);

    expect(arrangePlayerAction(p3, ActionType.CALL)).toBeTruthy();
    expectCardsFaceDown();
    expectPreflop();
    expectSubPots(122, 40);
    expectCurrentPlayer(p1);

    expect(arrangePlayerAction(p2, ActionType.FOLD)).toBeFalsy();
    expect(arrangePlayerAction(p3, ActionType.FOLD)).toBeFalsy();
    expect(arrangePlayerAction(p1, ActionType.CALL)).toBeTruthy();
    expectCardsFaceUp();
    expectFlop();
    expectSubPots(150, 20);
    expectBettingCycleOver();
    expectStack(p1, 0);
    expectStack(p2, 40);
    expectStack(p3, 0);
    expectHandId(1);
    
    tick();
    expectTurn();
    expectBettingCycleOver();
    expectStack(p1, 0);
    expectStack(p2, 40);
    expectStack(p3, 0);
    expectSubPots(150, 20);
    expectHandId(1);
    
    tick();
    expectRiver();
    expectBettingCycleOver();
    expectStack(p1, 0);
    expectStack(p2, 40);
    expectStack(p3, 0);
    expectSubPots(150, 20);
    expectHandId(1);
    expectStatusContains(/.*wins.*with/);

    tick();
    // expectHandId(0);
    expectPreflop();
  });

  test('2 players, check to showdown', () => {
    // enableLogging();
    const p1 = arrangePlayer(1);
    const p2 = arrangePlayer(2);
    game.addPlayer(p1);
    game.addPlayer(p2);

    game.start(gameOptions);
    expect(arrangePlayerAction(p2, ActionType.CALL)).toBeTruthy();
    expect(arrangePlayerAction(p1, ActionType.CHECK)).toBeTruthy();
    expectCardsFaceDown();
    expectFlop();
    expectSubPots(4);
    expectCurrentPlayer(p1);

    expect(arrangePlayerAction(p1, ActionType.CHECK)).toBeTruthy();
    expect(arrangePlayerAction(p2, ActionType.CHECK)).toBeTruthy();
    expectCardsFaceDown();
    expectTurn();
    expectSubPots(4);
    expectCurrentPlayer(p1);

    expect(arrangePlayerAction(p1, ActionType.CHECK)).toBeTruthy();
    expect(arrangePlayerAction(p2, ActionType.CHECK)).toBeTruthy();
    expectCardsFaceDown();
    expectRiver();
    expectSubPots(4);
    expectCurrentPlayer(p1);

    expect(arrangePlayerAction(p1, ActionType.CHECK)).toBeTruthy();
    expect(arrangePlayerAction(p2, ActionType.CHECK)).toBeTruthy();
    expectCardsFaceUp();
    expectRiver();
    expectStatusContains(/.*wins.*with/);
    expectSubPots(4);
    expectBettingCycleOver();

    tick();
    expectPreflop();
    expectHandId(2);
  });
});

function expectStatusContains(pattern: RegExp) {
  expect(getHandState().statusMessage).toMatch(pattern);
}

function expectSubPots(...amounts: number[]) {
  const subPots = getHandState().potState.subPots;
  expect(subPots).toEqual(amounts);
}

function expectActiveBet(player: Player, bet: number) {
  expect(getBettingPlayerState(player).activeBet).toEqual(bet);
}

function expectStack(player: Player, stack: number) {
  expect(player.state.stack).toEqual(stack);
}

function expectStackChange(player: Player, change: number) {
  expectStack(player, INITIAL_STACK + change);
}

function expectCurrentPlayer(player: Player) {
  expect(getBettingPlayerState(player).active).toBeTruthy();
}

function expectBettingCycleOver() {
  expect(getBettingState().bettingPlayerStates).toHaveLength(0);
}

function expectPreflop() {
  expect(getHandState().board).toHaveLength(0);
}

function expectFlop() {
  expect(getHandState().board).toHaveLength(3);
}

function expectTurn() {
  expect(getHandState().board).toHaveLength(4);
}

function expectRiver() {
  expect(getHandState().board).toHaveLength(5);
}

function expectCardsFaceDown() {
  expect(getHandState().cardsFaceUp).toBeFalsy();
}

function expectCardsFaceUp() {
  expect(getHandState().cardsFaceUp).toBeTruthy();
}

function expectPlayersInHand(...players: Player[]) {
  const state = getHandState();
  expect(state.handPlayers).toHaveLength(players.length);
  players.forEach(p => expect(getHandPlayerState(p)).toBeDefined());
}

function expectHandId(id: number) {
  expect(getHandState().handId).toEqual(id);
}

function getBettingPlayerState(player: Player): BettingPlayerState {
  return getBettingState().bettingPlayerStates.find(p => p.id === player.state.id);
}

function getBettingState(): BettingState {
  return getHandState().bettingState;
}

function getHandPlayerState(player: Player) {
  return game.getState().handState.handPlayers.find(p => p.id === player.state.id);
}

function getHandState(): HandState {
  return game.getState().handState;
}

function arrangePlayerAction(player: Player, actionType: ActionType, amount?: number): boolean {
  const playerAction: PlayerAction = {
    id: player.state.id,
    actionType,
    amount
  };
  return game.processPlayerAction(playerAction);
}

function arrangePlayer(id: number, initialStack?: number): Player {
  const name = 'p' + id;
  return new Player(name, name, nextSeat++, initialStack || INITIAL_STACK);
}

function tick() {
  executor.tick(AUTOPLAY_DELAY_MILLIS);
}
