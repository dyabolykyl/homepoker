import { SubPot } from '../../../src/host/game/SubPot';

describe('SubPot', () => {
  let sut: SubPot;
  const p1 = 'p1';
  const p2 = 'p2';
  const p3 = 'p3';

  beforeEach(() => {
    sut = new SubPot();
  });

  it('addBet, unlocked, expect full amount', () => {
    sut.addBet(p1, 500);
    sut.addBet(p2, 600);

    expect(sut.isLocked()).toBeFalsy();
    expect(sut.getContribution(p1)).toEqual(500);
    expect(sut.getContribution(p2)).toEqual(600);
    expect(sut.getContribution('p3')).toEqual(0);
    expect(sut.getTotalAmount()).toEqual(1100);
  });

  it('addBet, locked, expect limit added', () => {
    sut.addBet(p1, 500);
    sut.addBet(p2, 600);
    sut.lock(p1);

    sut.addBet(p3, 550);

    expect(sut.isLocked()).toBeTruthy();
    expect(sut.getContribution(p1)).toEqual(500);
    expect(sut.getContribution(p2)).toEqual(600);
    expect(sut.getContribution(p3)).toEqual(500);
    expect(sut.getTotalAmount()).toEqual(1600);
  });

  it('addBet, locked and small bet, expect bet added', () => {
    sut.addBet(p1, 500);
    sut.lock(p1);

    sut.addBet(p2, 400);

    expect(sut.isLocked()).toBeTruthy();
    expect(sut.getContribution(p1)).toEqual(500);
    expect(sut.getContribution(p2)).toEqual(400);
    expect(sut.getTotalAmount()).toEqual(900);
  });

  it('addBet, same player, expect throw', () => {
    sut.addBet(p1, 500);
    expect(() => sut.addBet(p1, 400)).toThrow();
  });
});
