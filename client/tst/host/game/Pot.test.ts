import { HandPlayer, HandPlayerStatus } from '../../../src/common/models/HandPlayer';
import { instance, mock, when } from 'ts-mockito';

import { Pot } from '../../../src/host/game/Pot';

const DEFAULT_STACK = 100;

describe('Pot', () => {
  let sut: Pot;
  let p1Mock: HandPlayer;
  let p2Mock: HandPlayer;
  let p3Mock: HandPlayer;
  let p1: HandPlayer;
  let p2: HandPlayer;
  let p3: HandPlayer;
  let players: HandPlayer[];

  beforeEach(() => {
    p1Mock = arrangeHandPlayer();
    p2Mock = arrangeHandPlayer();
    p3Mock = arrangeHandPlayer();
    p1 = instance(p1Mock);
    p2 = instance(p2Mock);
    p3 = instance(p3Mock);
    players = [p1, p2, p3];
    sut = new Pot(players);
  });

  it('create, expect no subpots', () => {
    expect(sut.getSubPots()).toHaveLength(0);
  });

  it('calculateSubPots, no bets, expect no subpots', () => {
    sut.calculateSubPots();
    expect(sut.getSubPots()).toHaveLength(0);
  });

  it('calculateSubPots, 1 better, expect 1 subpot', () => {
    when(p1Mock.totalBet).thenReturn(50);

    sut.calculateSubPots();
    
    expect(sut.getTotalAmount()).toEqual(50);
    const subPots = sut.getSubPots();
    expect(subPots).toHaveLength(1);
    expect(subPots[0].isLocked()).toBeFalsy();
    expect(subPots[0].getContribution(p1.id)).toEqual(50);
  });

  it('calculateSubPots, 2 betters, 1 folded, expect 1 subpot', () => {
    when(p1Mock.totalBet).thenReturn(50);
    when(p2Mock.totalBet).thenReturn(25);
    when(p2Mock.status).thenReturn(HandPlayerStatus.OUT);

    sut.calculateSubPots();
    
    expect(sut.getTotalAmount()).toEqual(75);
    const subPots = sut.getSubPots();
    expect(subPots).toHaveLength(1);
    expect(subPots[0].isLocked()).toBeFalsy();
    expect(subPots[0].getContribution(p1.id)).toEqual(50);
    expect(subPots[0].getContribution(p2.id)).toEqual(25);
    expect(subPots[0].getContribution(p3.id)).toEqual(0);
  });

  it('calculateSubPots, 2 betters, 2 small stacks, 1 fold, expect 2 subpots', () => {
    when(p2Mock.initialStack).thenReturn(50);
    when(p1Mock.totalBet).thenReturn(75);
    when(p2Mock.totalBet).thenReturn(50);
    when(p3Mock.status).thenReturn(HandPlayerStatus.OUT);
    when(p3Mock.totalBet).thenReturn(25);

    sut.calculateSubPots();
    
    expect(sut.getTotalAmount()).toEqual(150);
    const subPots = sut.getSubPots();
    expect(subPots).toHaveLength(2);
    expect(subPots[0].isLocked()).toBeTruthy();
    expect(subPots[0].getContribution(p1.id)).toEqual(50);
    expect(subPots[0].getContribution(p2.id)).toEqual(50);
    expect(subPots[0].getContribution(p3.id)).toEqual(25);
    expect(subPots[1].getContribution(p1.id)).toEqual(25);
    expect(subPots[1].getContribution(p2.id)).toEqual(0);
    expect(subPots[1].getContribution(p3.id)).toEqual(0);
  });

  it('calculateSubPots, 2 betters, 1 small stack, 1 fold, expect 2 subpots', () => {
    when(p2Mock.initialStack).thenReturn(50);
    when(p1Mock.totalBet).thenReturn(75);
    when(p2Mock.totalBet).thenReturn(50);
    when(p3Mock.status).thenReturn(HandPlayerStatus.OUT);
    when(p3Mock.totalBet).thenReturn(60);

    sut.calculateSubPots();
    
    expect(sut.getTotalAmount()).toEqual(185);
    const subPots = sut.getSubPots();
    expect(subPots).toHaveLength(2);
    expect(subPots[0].isLocked()).toBeTruthy();
    expect(subPots[0].getContribution(p1.id)).toEqual(50);
    expect(subPots[0].getContribution(p2.id)).toEqual(50);
    expect(subPots[0].getContribution(p3.id)).toEqual(50);
    expect(subPots[1].getContribution(p1.id)).toEqual(25);
    expect(subPots[1].getContribution(p2.id)).toEqual(0);
    expect(subPots[1].getContribution(p3.id)).toEqual(10);
  });
});

let identifier = 0;
function arrangeHandPlayer(): HandPlayer {
  const player = mock(HandPlayer);
  when(player.id).thenReturn(`p${identifier++}`);
  when(player.status).thenReturn(HandPlayerStatus.IN);
  when(player.initialStack).thenReturn(DEFAULT_STACK);
  return player;
}