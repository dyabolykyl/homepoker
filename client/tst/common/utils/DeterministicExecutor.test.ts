import { DeterministicExecutor } from '../../../src/common/utils/Executor';

describe('DeterministicExecutor', () => {
  let sut: DeterministicExecutor;

  beforeEach(() => {
    sut = new DeterministicExecutor();
  });

  it('submit, no delay, executes immediately', () => {
    const action = jest.fn();
    sut.submit(action);
    expect(action).toHaveBeenCalledTimes(1);
  });

  it('submit, delay, executes after delay', () => {
    const action = jest.fn();
    sut.submit(action, 10);
    expect(action).toHaveBeenCalledTimes(0);

    sut.tick(9);
    expect(action).toHaveBeenCalledTimes(0);

    sut.tick(1);
    expect(action).toHaveBeenCalledTimes(1);
  });

  it('submit, action that submits another action', () => {
    const action = jest.fn();
    const delayer = () => sut.submit(action, 20);
    sut.submit(delayer, 10);
    expect(action).toHaveBeenCalledTimes(0);

    sut.tick(10);
    expect(action).toHaveBeenCalledTimes(0);

    sut.tick(19);
    expect(action).toHaveBeenCalledTimes(0);

    sut.tick(1);
    expect(action).toHaveBeenCalledTimes(1);
  });
});
